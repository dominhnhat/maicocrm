﻿

START TRANSACTION;

ALTER TABLE "LegalRecords" DROP COLUMN name;

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20210811045450_DeleteLegalName', '5.0.8');

COMMIT;

