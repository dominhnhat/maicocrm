﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using LinqKit;
using MudBlazor;

namespace maicoCRM.Services
{
    public class OrderServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        private ExpressionStarter<Order> _filterCondition;
        public OrderServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public Order GetByCustomerId(int customerId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Orders.Where(x => x.CustomerId == customerId)
                .Include(x => x.CustomerNavigation)
                .Include(x => x.EmployeeNavigation)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.EmployeeNavigation)
                .Include(x => x.OrderReceivedFees)
                .AsSplitQuery()
                .OrderByDescending(x => x.CreatedAt).FirstOrDefault();
        }
        public async Task<Order> GetByCustomerIdAsync(int customerId)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Orders.Where(x => x.CustomerId == customerId)
                .Include(x => x.CustomerNavigation)
                .Include(x => x.EmployeeNavigation)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.EmployeeNavigation)
                .Include(x => x.OrderReceivedFees)
                .OrderByDescending(x => x.CreatedAt).FirstOrDefaultAsync();
        }
        public async Task<List<Order>> GetAllByCustomerIdAsync(int customerId)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Orders.Where(x => x.CustomerId == customerId)
                .Include(x => x.EmployeeNavigation)
                .OrderByDescending(x => x.CreatedAt).ToListAsync();
        }
        public void Update(Order order)
        {
            try
            {
                using var context = _contextFactory.CreateDbContext();
                context.Orders.Update(order);
                context.SaveChanges();
                foreach (var item in order.OrderDetails)
                {
                    context.Entry(item).Reference(c => c.EmployeeNavigation).Load();
                }
            }
            catch (InvalidOperationException)
            {
                throw new DbUpdateException("Duplicate order detail");
            }
        }
        public void UpdateOrderOnly(Order order)
        {
            var orderDetails = order.OrderDetails;
            order.OrderDetails = null;
            using var context = _contextFactory.CreateDbContext();
            context.Orders.Update(order);
            context.SaveChanges();
            order.OrderDetails = orderDetails;
        }
        public async Task AddAsync(Order order)
        {
            using var context = _contextFactory.CreateDbContext();
            try
            {
                context.Orders.Add(order);
            }
            catch (InvalidOperationException)
            {
                throw new DbUpdateException("Duplicate Id");
            }

            await context.SaveChangesAsync();

            context.Entry(order).Reference(r => r.EmployeeNavigation).Load();
            context.Entry(order).Reference(r => r.CustomerNavigation).Load();
            context.Entry(order).Reference(r => r.ProjectNavigation).Load();

            foreach (var item in order.OrderDetails)
            {
                context.Entry(item).Reference(r => r.EmployeeNavigation).Load();
            }
        }
        public void Delete(Order order)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Orders.Remove(order);
            context.SaveChanges();
        }
        public Order GetOrderById(int orderId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Orders.Where(x => x.Id == orderId)
                .Include(x => x.CustomerNavigation)
                .Include(x => x.EmployeeNavigation)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.OrderComments)
                .Include(x => x.OrderReceivedFees)
                    .ThenInclude(x => x.CollecterNavigation)
                .Include(x => x.OrderReceivedFees)
                    .ThenInclude(x => x.ReceiverNavigation)
                .Include(x => x.OrderReceivedFees)
                    .ThenInclude(x => x.CreaterNavigation)
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.EmployeeNavigation)
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.OrderJobTypes)
                .Include(x => x.LegalRecord)
                .AsSplitQuery()
                .FirstOrDefault();
        }
        public void UpdateLog(int orderId, UpdateOrderLog updateOrderLog)
        {
            using var context = _contextFactory.CreateDbContext();
            var findOrder = context.Orders.FirstOrDefault(x => x.Id == orderId);
            findOrder.Logs.Add(updateOrderLog);
            context.Update(findOrder);
            context.SaveChanges();
        }
        public async Task<List<Order>> GetByPage(int page)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Orders
                .Include(x => x.CustomerNavigation)
                .Include(x => x.EmployeeNavigation)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.OrderReceivedFees)
                .OrderByDescending(x => x.CreatedAt)
                .Skip((page / 10) * 100)
                .Take(100)
                .ToListAsync();
        }
        public async Task<List<Order>> GetAll()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Orders
               .Include(x => x.CustomerNavigation)
               .Include(x => x.EmployeeNavigation)
               .Include(x => x.ProjectNavigation)
               .Include(x => x.OrderReceivedFees)
               .ToListAsync();
        }
        public async Task<int> OrderCount()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Orders.CountAsync();
        }
        public async Task<(int, List<Order>)> FilterByPage(int page, string need, int? employeeId, DateRange dateRange, int? projectId, bool? isCofirm, string searchString, string source)
        {
            _filterCondition = PredicateBuilder.New<Order>();

            if (String.IsNullOrEmpty(need) &&
                String.IsNullOrEmpty(searchString) &&
                (employeeId == null || employeeId == 0) &&
                (projectId == null || projectId == 0)
                && isCofirm == null && dateRange == null
                && string.IsNullOrEmpty(source))
            {
                _filterCondition.And(c => true);
            }
            else
            {
                if (!String.IsNullOrEmpty(need)) _filterCondition.And(c => c.ContractType == need);
                if (!String.IsNullOrEmpty(searchString))
                {
                    var tempFilter = PredicateBuilder.New<Order>();
                    searchString = searchString?.Insert(0, " ");
                    searchString += " ";
                    searchString = searchString?.Replace(" ", "%");

                    tempFilter.Or(c => EF.Functions.ILike(c.ContractType, searchString));
                    tempFilter.Or(c => EF.Functions.ILike(c.CustomerNavigation.Name, searchString));
                    tempFilter.Or(c => EF.Functions.ILike(c.ProjectNavigation.Name, searchString));
                    tempFilter.Or(c => c.OrderDetails.Any(e => EF.Functions.ILike(e.EmployeeNavigation.Name, searchString)));
                    _filterCondition.And(tempFilter);
                }
                if (employeeId != null && employeeId != 0) _filterCondition.And(c => c.OrderDetails.Any(e => e.EmployeeId == employeeId));
                if (dateRange != null) _filterCondition.And(c => c.CreatedAt >= dateRange.Start.Value && c.CreatedAt < dateRange.End.Value.AddDays(1));
                if (projectId != null && projectId != 0) _filterCondition.And(c => c.ProjectId == projectId);
                if (isCofirm != null) _filterCondition.And(c => c.IsConfirmed == isCofirm);
                if (!string.IsNullOrEmpty(source))
                {
                    _filterCondition.And(c => c.CustomerNavigation.SheetMarketings.Where(c => c.Source == source).Any());
                }
            }
            using var context = _contextFactory.CreateDbContext();
            return (context.Orders.Where(_filterCondition).Count(), await context.Orders.Where(_filterCondition)
                                                                                         .Include(c => c.OrderDetails)
                                                                                            .ThenInclude(c => c.EmployeeNavigation)
                                                                                         .Include(c => c.ProjectNavigation)
                                                                                         .Include(c => c.CustomerNavigation)
                                                                                         .Include(c => c.OrderReceivedFees)
                                                                                         .OrderByDescending(c => c.CreatedAt)
                                                                                         .Skip((page / 10) * 100)
                                                                                         .Take(100)
                                                                                         .AsSplitQuery()
                                                                                         .ToListAsync());
        }
        public async Task<List<Order>> GetByDateRange(DateTime dateStart, DateTime dateEnd)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Orders
                .Where(c => c.CreatedAt >= dateStart && c.CreatedAt < dateEnd.AddDays(1))
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.EmployeeNavigation)
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.OrderJobTypes)
                .Include(c => c.ProjectNavigation)
                .ToListAsync();
        }
        public async Task<List<Order>> GetByDateRange2(DateTime dateStart, DateTime dateEnd)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Orders
                .Where(c => c.CreatedAt >= dateStart && c.CreatedAt < dateEnd.AddDays(1))
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.EmployeeNavigation)
                .Include(x => x.OrderDetails)
                    .ThenInclude(x => x.OrderJobTypes)
                .Include(c => c.ProjectNavigation)
                .Include(c => c.CustomerNavigation)
                .ToListAsync();
        }
        public double GetLegalReportFeeSummary(DateTime month)
        {
            month = new DateTime(month.Year, month.Month, 5);
            using var context = _contextFactory.CreateDbContext();
            return context.Orders.Where(c => c.CreatedAt.Date >= month.Date && c.CreatedAt.Date < month.AddMonths(1).Date)
                            .Sum(c => c.LegalReportsFee);
        }
        public double GetLegalReportFeeSummary(DateTime timeStart, DateTime timeEnd)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Orders.Where(c => c.CreatedAt.Date > timeStart.Date && c.CreatedAt.Date <= timeEnd.Date)
                            .Sum(c => c.LegalReportsFee);
        }
        public bool FilterDateTimeStart(DateTime date1, DateTime date2)
        {
            int result = DateTime.Compare(date1, date2);
            if (result >= 0)
                return true;
            else if (result < 0)
                return false;
            return true;
        }
        public bool FilterDateTimeEnd(DateTime date1, DateTime date2)
        {
            int result = DateTime.Compare(date1, date2);
            if (result <= 0)
                return true;
            else if (result > 0)
                return false;
            return true;
        }
        public List<Order> GetInvestOnMonth(DateTime month)
        {
            month = new DateTime(month.Year, month.Month, 5);

            DateTime start = month;
            var temp = month.AddMonths(1);
            DateTime end = new DateTime(temp.Year, temp.Month, 5);

            using var context = _contextFactory.CreateDbContext();
            return context.Orders
                .Where(x => x.CreatedAt.Date >= start.Date && x.CreatedAt.Date < end.Date)
               .Include(x => x.CustomerNavigation)
               .Include(x => x.EmployeeNavigation)
               .Include(x => x.ProjectNavigation)
               .Include(x => x.OrderReceivedFees)
               .Where(x => x.IsActive == true && x.IsConfirmed == true && x.IsInvestmentOrder == true)
               .ToList();
        }
    }

}
