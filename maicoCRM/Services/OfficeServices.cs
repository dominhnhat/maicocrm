﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MudBlazor;

namespace maicoCRM.Services
{
    public class OfficeServices
    {
        private readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        
        public OfficeServices(IDbContextFactory<MaicoCRMContext> dbContextFactory)
        {
            _contextFactory = dbContextFactory;
        }

        public async  Task<List<Office>> GetSalesByOffice(DateRange dateRange)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Offices.Where(x => x.Id > 1)
                                        .Include(x => x.EmployeesNav)
                                            .ThenInclude(x => x.OrderDetails.Where(c => c.OrderNavigation.CreatedAt.Date >= dateRange.Start.Value.Date
                                                                                        && c.OrderNavigation.CreatedAt.Date <= dateRange.End.Value.Date 
                                                                                        && c.OrderNavigation.IsConfirmed == true
                                                                                        && c.OrderNavigation.IsActive == true))
                                                .ThenInclude(x => x.OrderJobTypes)
                                        .Include(x => x.EmployeesNav)
                                            .ThenInclude(x => x.OrderDetails.Where(c => c.OrderNavigation.CreatedAt.Date >= dateRange.Start.Value.Date
                                                                                        && c.OrderNavigation.CreatedAt.Date <= dateRange.End.Value.Date
                                                                                        && c.OrderNavigation.IsConfirmed == true
                                                                                        && c.OrderNavigation.IsActive == true))
                                                .ThenInclude(x => x.OrderNavigation)
                                                    .ThenInclude(x => x.ProjectNavigation)
                                        .ToListAsync();
        }
        public async Task<List<Office>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Offices.Skip(1).ToListAsync();
        }
        public async Task<List<Office>> GetMainAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Offices.Where(x => x.Id > 1).ToListAsync();
        }
    }
}
