﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;
using OfficeOpenXml;
using Microsoft.JSInterop;
using maicoCRM.Models;
using OfficeOpenXml.Style;
using System.Drawing;

namespace maicoCRM.Services
{
    public class PrepareExcel
    {
        private List<Models.Order> orders { get; set; }
        private OrderJobTypeServices orderJobTypeServices { get; set; }
        public PrepareExcel(List<Models.Order> _orders, OrderJobTypeServices _orderJobTypeServices)
        {
            orders = _orders;
            orderJobTypeServices = _orderJobTypeServices;
        }
        public Task GenerateExcel(IJSRuntime iJSRuntime)
        {
            Task task = new(() =>
            {
                byte[] fileContents;
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage())
                {
                    var workSheet = package.Workbook.Worksheets.Add("Đơn Hàng MCG");
                    workSheet.Cells[1, 1].Value = "Ngày đặt hàng";
                    workSheet.Cells[1, 2].Value = "Mã Đơn hàng";
                    workSheet.Cells[1, 3].Value = "Tên khách hàng";
                    workSheet.Cells[1, 4].Value = "Số điện thoại";
                    workSheet.Cells[1, 5].Value = "Tên sản phẩm";
                    workSheet.Cells[1, 6].Value = "Bán/Thuê";
                    workSheet.Cells[1, 7].Value = "Người thực hiện";
                    workSheet.Cells[1, 8].Value = "Doanh số";
                    workSheet.Cells[1, 9].Value = "Đã thanh toán";
                    workSheet.Cells[1, 10].Value = "Còn lại";
                    workSheet.Cells[1, 11].Value = "Ngày thanh toán";
                    var numberFormat = "#.##0";
                    var dataCellStyleName = "TableNumber";
                    var numStyle = package.Workbook.Styles.CreateNamedStyle(dataCellStyleName);
                    numStyle.Style.Numberformat.Format = numberFormat;
                    int rowIndex = 2;
                    foreach (var item in orders)
                    {
                        IEnumerable<String> employees = item.OrderDetails.Select(c => c.EmployeeNavigation.Name);
                        int numerable = -1;
                        string createdAt = item.CreatedAt.ToString("dd/MM/yyyy");
                        string itemId = "DH" + item.Id;
                        string customerName = item.CustomerNavigation.Name;
                        string[] phones = item.CustomerNavigation.PhoneNumber.ToArray();
                        string customerPhone = "";
                        if (phones.Length > 0)
                        {
                            customerPhone = phones[0];
                        }
                        else
                        {
                            customerPhone = "Không có số";
                        }
                        string projectName = item.ProjectNavigation.Name;
                        string projectType = "";
                        if (item.ContractType == "Mua")
                        {
                            projectType = "Hợp đồng bán";
                        }
                        else
                        {
                            projectType = "Hợp đồng thuê";
                        }
                        double SumReceivedFee = item.OrderReceivedFees.Sum(c => c.ReceivedFee);
                        string dateOfPayment = "";
                        DateTime[] datePayments = item.OrderReceivedFees.Select(c => c.CreatedAt).ToArray();
                        if (datePayments.Length != 0)
                        {
                            dateOfPayment = datePayments[datePayments.Length - 1].ToString("dd/MM/yyyy");
                        }

                        foreach (var employee in employees)
                        {
                            numerable++;
                            workSheet.Cells[rowIndex, 1].Value = createdAt;
                            workSheet.Cells[rowIndex, 2].Value = itemId;
                            workSheet.Cells[rowIndex, 3].Value = customerName;
                            workSheet.Cells[rowIndex, 4].Value = customerPhone;
                            workSheet.Cells[rowIndex, 5].Value = projectName;
                            workSheet.Cells[rowIndex, 6].Value = projectType;
                            workSheet.Cells[rowIndex, 7].Value = employee;
                            double percent = orderJobTypeServices.GetByOrderDetailId(item.OrderDetails[numerable].Id).Sum(c => c.PercentContribute);
                            double totalMoneyPerson = ((item.BrokerageFee / 100) * percent);
                            double receiveMoneyPerson = ((SumReceivedFee / 100) * percent);
                            workSheet.Cells[rowIndex, 8].Value = "" + totalMoneyPerson;
                            workSheet.Cells[rowIndex, 9].Value = "" + receiveMoneyPerson;
                            workSheet.Cells[rowIndex, 10].Value = "" + (totalMoneyPerson - receiveMoneyPerson);
                            workSheet.Cells[rowIndex, 11].Value = dateOfPayment;
                            rowIndex++;
                        }
                        if (item.ContractType == "Mua")
                        {
                            workSheet.Cells[rowIndex, 1].Value = createdAt;
                            workSheet.Cells[rowIndex, 2].Value = itemId;
                            workSheet.Cells[rowIndex, 3].Value = customerName;
                            workSheet.Cells[rowIndex, 4].Value = customerPhone;
                            workSheet.Cells[rowIndex, 5].Value = projectName;
                            workSheet.Cells[rowIndex, 6].Value = "Hợp đồng bán";
                            workSheet.Cells[rowIndex, 7].Value = "Pháp lý";
                            workSheet.Cells[rowIndex, 8].Value = "" + item.LegalReportsFee;
                            workSheet.Cells[rowIndex, 10].Value = "" + item.LegalReportsFee;
                            rowIndex++;
                        }
                    }
                    using (var range = workSheet.Cells[1, 1, rowIndex - 1, 11])
                    {
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                        range.Style.Font.Color.SetColor(Color.FromArgb(1, 1, 1));
                        range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                    using (var range = workSheet.Cells[1, 1, 1, 11])
                    {
                        range.Style.Font.Size = 12;
                        range.Style.Font.Bold = true;
                        range.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 0));
                    }
                    fileContents = package.GetAsByteArray();
                }
                iJSRuntime.InvokeAsync<PrepareExcel>("saveAsFile"
                                                    , "Đơn hàng MCG.xlsx"
                                                    , Convert.ToBase64String(fileContents));
            });

            task.Start();
            return task;
        }
    }
    public class Utilities
    {
        public TimeZoneInfo TimeZone { get; set; } = null;

        public Utilities()
        {
            // Determine if South Pole time zone is defined in system
            try
            {
                TimeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            }
            // Time zone does not exist; create it, store it in a text file, and return it
            catch
            {
                const string filename = "TimeZoneInfo.txt";
                bool found = false;

                if (File.Exists(filename))
                {
                    StreamReader reader = new StreamReader(filename);
                    string timeZoneInfo;
                    while (reader.Peek() >= 0)
                    {
                        timeZoneInfo = reader.ReadLine();
                        if (timeZoneInfo.Contains("SE Asia Standard Time"))
                        {
                            TimeZone = TimeZoneInfo.FromSerializedString(timeZoneInfo);
                            reader.Close();
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    // Define transition times to/from DST
                    TimeZoneInfo.TransitionTime startTransition = TimeZoneInfo.TransitionTime.CreateFloatingDateRule(new DateTime(1, 1, 1, 2, 0, 0), 10, 1, DayOfWeek.Sunday);
                    TimeZoneInfo.TransitionTime endTransition = TimeZoneInfo.TransitionTime.CreateFloatingDateRule(new DateTime(1, 1, 1, 2, 0, 0), 3, 3, DayOfWeek.Sunday);
                    // Define adjustment rule
                    TimeSpan delta = new TimeSpan(1, 0, 0);
                    TimeZoneInfo.AdjustmentRule adjustment = TimeZoneInfo.AdjustmentRule.CreateAdjustmentRule(new DateTime(1989, 10, 1), DateTime.MaxValue.Date, delta, startTransition, endTransition);
                    // Create array for adjustment rules
                    TimeZoneInfo.AdjustmentRule[] adjustments = { adjustment };
                    // Define other custom time zone arguments
                    string displayName = "(GMT+7:00) SE Asia Standard Time";
                    string standardName = "SE Asia Standard Time";
                    string daylightName = "SE Asia Daylight Time";
                    TimeSpan offset = new TimeSpan(7, 0, 0);
                    TimeZone = TimeZoneInfo.CreateCustomTimeZone(standardName, offset, displayName, standardName, daylightName, adjustments);
                    // Write time zone to the file
                    StreamWriter writer = new StreamWriter(filename, true);
                    writer.WriteLine(TimeZone.ToSerializedString());
                    writer.Close();
                }
            }
        }
        public bool IsPhoneNumber(string number)
        {
            //string MatchPhoneNumberPattern = @"^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$";
            string matchPhoneNumberPattern = @"^\(?(?:\+?\d{2,3})\)?\d{8,13}$";
            if (number != null)
            {
                return Regex.IsMatch(number.Trim(), matchPhoneNumberPattern);
            }
            return false;
        }
        public bool IsEqual(object firstOrder, object secondOrder)
        {
            var jsonOfOrder = JsonConvert.SerializeObject(firstOrder, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            var jsonOfTempOrder = JsonConvert.SerializeObject(secondOrder, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            return jsonOfOrder == jsonOfTempOrder;
        }

        public DateTime GetCurrentDateTime()
        {
            DateTime dateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZone);
            return dateTime;
        }
    }
}
