﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using MudBlazor;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;


namespace maicoCRM.Services
{
    public class DocumentServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public DocumentServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public List<Document> GetAll()
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Documents.ToList();
        }
        public async Task<List<Document>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Documents.ToListAsync();
        }
        public async Task<List<Document>> GetAllByDocumentTypeAsync(string DocumentType)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Documents
                .Where(c => c.DocumentType == DocumentType)
                .ToListAsync();
        }
        public Document GetById(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Documents.Where(c => c.Id == id).FirstOrDefault();
        }
        public string GetName(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Documents.Where(x => x.Id == id).FirstOrDefault().Name;
        }
        public void Add(Document document)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Documents.Add(document);
            context.SaveChanges();
        }
        public void Delete(Document document)
        {
            using var context = _contextFactory.CreateDbContext();
            try
            {
                context.Remove(document);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public void UpdateType(string OldDocumentType, string NewDocumentType)
        {
            using var context = _contextFactory.CreateDbContext();
            List<Document> documents = context.Documents.Where(c => c.DocumentType == OldDocumentType).ToList();
            foreach (var document in documents)
            {
                document.DocumentType = NewDocumentType;
                context.Documents.Update(document);
            }
            context.SaveChanges();
        }
        public void UpdateName(Document document)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Documents.Update(document);
            context.SaveChanges();
        }
    }
}