﻿using maicoCRM.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using maicoCRM.Models;
namespace maicoCRM.Services
{
    public class OrderJobTypeServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public OrderJobTypeServices(IDbContextFactory<MaicoCRMContext> contexetFactory)
        {
            _contextFactory = contexetFactory;
        }
        public void RemoveAllByOrder(Order order)
        {
            using var context = _contextFactory.CreateDbContext();
            foreach(var orderDetail in order.OrderDetails)
            {
                var findJobType = context.OrderJobTypes.Where(x => x.OrderDetailId == orderDetail.Id).ToList();
                context.OrderJobTypes.RemoveRange(findJobType);
            }
            context.SaveChanges();
        }
        public List<OrderJobType> GetByOrderDetailId(int OrderDetailId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.OrderJobTypes.Where(x=>x.OrderDetailId==OrderDetailId)
                .Include(x => x.OrderDetailNavigation)
                .ToList();
        }
        public void AddRange(List<OrderJobType> listJobType)
        {
            using var context = _contextFactory.CreateDbContext();
            context.OrderJobTypes.AddRange(listJobType);
            context.SaveChanges();
        }
    }
}
