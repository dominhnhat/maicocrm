﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using maicoCRM.Data;
using Microsoft.EntityFrameworkCore;
using maicoCRM.Models;

namespace maicoCRM.Services
{
    public class OrderCommentServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        Utilities _utilities;
        public OrderCommentServices(IDbContextFactory<MaicoCRMContext> contextFactory, Utilities utilities)
        {
            _contextFactory = contextFactory;
            _utilities = utilities;
        }
        public void Add(OrderComment orderComment)
        {
            DateTime now = _utilities.GetCurrentDateTime();

            using var context = _contextFactory.CreateDbContext();
            orderComment.CreatedAt = now;
            context.OrderComments.Add(orderComment);
            context.Entry(orderComment).Reference(c => c.EmployeeNavigation).Load();
            context.SaveChanges();
        }
    }
}
