﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using maicoCRM.Models;
using maicoCRM.Data;

namespace maicoCRM.Services
{
    public class WeeklySalesReportServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;

        public WeeklySalesReportServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        
        public async Task AddRangeAsync(List<WeeklySalesReport> rps)
        {
            using var context = _contextFactory.CreateDbContext();
            context.WeeklySalesReports.AddRange(rps);
            await context.SaveChangesAsync();
        }
        public void  Add(WeeklySalesReport weeklySalesReport)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Add(weeklySalesReport);
            context.SaveChanges();

            context.Entry(weeklySalesReport).Reference(c => c.DurationNavigation).Load();
        }
    }
}
