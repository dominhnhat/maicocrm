﻿using maicoCRM.Component.Shared.Notification;
using maicoCRM.Models;
using MudBlazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using maicoCRM.Services;

namespace maicoCRM.Services
{
    public class ScheduleNotificationServices
    {
        Timer _timerNotification;
        Utilities _utilities;

        public ScheduleNotificationServices(Utilities utilities)
        {
            _utilities = utilities;
            _timerNotification = new(1000 * 60);
            _timerNotification.Elapsed += OnTimedEventShowNotification;
            _timerNotification.AutoReset = true;
            _timerNotification.Start();
        }
        public event Action<ScheduleCustomer> OnElapsedShowNotification;

        List<ScheduleCustomer> _calendars = new();
        public List<ScheduleCustomer> Calendars
        {
            get
            {
                return _calendars;
            }
            set
            {
                _calendars = value;
                FilterCalendarToday();
            }
        }
        List<ScheduleCustomer> _calendarToday = new();

        double _minuteNotification;
        public double MinuteNotification
        {
            get
            {
                return _minuteNotification;
            }
            set
            {
                _minuteNotification = value;
            }
        }

        public void AddCalendar(ScheduleCustomer calendar)
        {
            _calendars.Add(calendar);
            FilterCalendarToday();
        }
        public void UpdateCalendar(ScheduleCustomer calendar)
        {
            int indexCalendarInList = _calendars.FindIndex(c => c.Id == calendar.Id);
            _calendars[indexCalendarInList] = calendar;
            FilterCalendarToday();
        }

        private void FilterCalendarToday()
        {
            _calendarToday = _calendars
                .Where(c => c.TimeStart.Date.ToShortDateString() == _utilities.GetCurrentDateTime().Date.ToShortDateString())
                .OrderBy(c => c.TimeStart)
                .ToList();
        }

        private void OnTimedEventShowNotification(Object source, ElapsedEventArgs e)
        {
            DateTime dateTime = e.SignalTime.AddHours(7);
            var schedule = _calendarToday.FirstOrDefault(c => c.TimeStart.AddMinutes(-_minuteNotification).ToString("dd-MM-yy HH:mm") == dateTime.ToString("dd-MM-yy HH:mm"));
            if(schedule != null)
            {
                OnElapsedShowNotification?.Invoke(schedule);
            }
        }
    }
}
