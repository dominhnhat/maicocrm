﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class ScheduleCustomerServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        Utilities _utilities;
        public ScheduleCustomerServices(IDbContextFactory<MaicoCRMContext> context, Utilities utilities)
        {
            _contextFactory = context;
            _utilities = utilities;
        }

        public List<ScheduleCustomer> GetAll()
        {
            using var context = _contextFactory.CreateDbContext();
            return context.ScheduleCustomers.ToList();
        }

        public void Add( ScheduleCustomer scheduleCustomer)
        {
            using var context = _contextFactory.CreateDbContext();
            context.ScheduleCustomers.Add(scheduleCustomer);
            context.SaveChanges();
            context.Entry(scheduleCustomer).Reference(r => r.CreatorNavigation).Load();
            context.Entry(scheduleCustomer).Reference(r => r.CustomerNavigation).Load();
            context.Entry(scheduleCustomer).Reference(r => r.ProjectNavigation).Load();
            context.Entry(scheduleCustomer).Reference(r => r.ExecutorNavigation).Load();
        }
        public async Task AddAsync(ScheduleCustomer scheduleCustomer)
        {
            using var context = _contextFactory.CreateDbContext();
            context.ScheduleCustomers.Add(scheduleCustomer);
            await context.SaveChangesAsync();
            context.Entry(scheduleCustomer).Reference(r => r.CreatorNavigation).Load();
            context.Entry(scheduleCustomer).Reference(r => r.CustomerNavigation).Load();
            context.Entry(scheduleCustomer).Reference(r => r.ProjectNavigation).Load();
            context.Entry(scheduleCustomer).Reference(r => r.ExecutorNavigation).Load();
        }
        public List<ScheduleCustomer> GetAllByCustomerId(int customerId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.ScheduleCustomers
                    .Include(x => x.CustomerNavigation)
                    .Include(x => x.CreatorNavigation)
                    .Include(x => x.ProjectNavigation)
                    .Include(x => x.ExecutorNavigation)
                    .Where(x => x.CustomerId == customerId).ToList(); 
        }
        public async Task<List<ScheduleCustomer>> GetAllByCustomerIdAsync(int customerId)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.ScheduleCustomers
                    .Include(x => x.CustomerNavigation)
                    .Include(x => x.CreatorNavigation)
                    .Include(x => x.ProjectNavigation)
                    .Include(x => x.ExecutorNavigation)
                    .AsSplitQuery()
                    .Where(x => x.CustomerId == customerId).ToListAsync();
        }
        public void Update(ScheduleCustomer scheduleCustomer)
        {
            using var context = _contextFactory.CreateDbContext();
            context.ScheduleCustomers.Update(scheduleCustomer);
            context.SaveChanges();
        }
        public async Task UpdateAsync(ScheduleCustomer scheduleCustomer)
        {
            using var context = _contextFactory.CreateDbContext();
            context.ScheduleCustomers.Update(scheduleCustomer);
            await context.SaveChangesAsync();
        }
        public ScheduleCustomer GetByCustomerId(int customerId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.ScheduleCustomers
                    .Include(x => x.CustomerNavigation)
                    .Include(x => x.CreatorNavigation)
                    .Include(x => x.ProjectNavigation)
                    .Include(x => x.ExecutorNavigation)
                    .Where(x => x.CustomerId == customerId).FirstOrDefault(); 
        } 
        public int GetNumberOfSuccessHomeView(int CustomerId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.ScheduleCustomers.Where(x => x.Status == "Thành Công" && x.CustomerId == CustomerId).Count();
        }
        public List<(int CustomerId, string CustomerName, DateTime TimeStart, string JobType)> GetScheduleByCurrentDay(int employeeId)
        {
            DateTime now = _utilities.GetCurrentDateTime();

            using var context = _contextFactory.CreateDbContext();
            return context.ScheduleCustomers
                            .Include(c => c.CustomerNavigation)
                            .Where(c => (EF.Functions.JsonContains(c.Followers, $"[{{\"Id\":{employeeId}}}]") || c.ExecutorId == employeeId)
                                        && c.TimeStart.Date == now.Date
                                        && c.Status.Contains("Đợi"))
                            .AsEnumerable()
                            .Select(c => (CustomerId : c.CustomerId, CustomerName: c.CustomerNavigation.Name, TimeStart: c.TimeStart, JobType: c.JobType))
                            .OrderBy(c => c.TimeStart)
                            .ToList();
        } 
        public async Task<List<ScheduleCustomer>> GetCalendarData(int? employeeId)
        {
            using var context = _contextFactory.CreateDbContext();
            if(employeeId != null)
            {
                return await context.ScheduleCustomers
                       .Include(c => c.CustomerNavigation)
                       .Include(c => c.ProjectNavigation)
                       .Where(c => EF.Functions.JsonContains(c.Followers, $"[{{\"Id\":{employeeId}}}]") ||
                                   c.ExecutorId == employeeId)
                       .ToListAsync();
            }
            else
            { 
                return await context.ScheduleCustomers
                           .Include(c => c.CustomerNavigation)
                           .Include(c => c.ProjectNavigation)
                           .ToListAsync();
            }
        }
        public async Task<List<ScheduleCustomer>> GetCalendarData(int employeeId, int take)
        {
            using var context = _contextFactory.CreateDbContext();

            return await context.ScheduleCustomers
                    .Include(c => c.CustomerNavigation)
                    .Include(c => c.ProjectNavigation)
                    .Where(c => EF.Functions.JsonContains(c.Followers, $"[{{\"Id\":{employeeId}}}]") ||
                                c.ExecutorId == employeeId)
                    .OrderByDescending(c => c.TimeStart)
                    .Take(take)
                    .ToListAsync();
            
        }

        public async Task<List<ScheduleCustomer>> GetWorkDashboardData(DateTime start, DateTime end)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.ScheduleCustomers
                .Where(x => x.TimeEnd.Date >= start.Date && x.TimeEnd.Date <= end.Date)
                .ToListAsync();
        }
    }
}
