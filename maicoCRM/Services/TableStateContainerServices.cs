﻿using MudBlazor;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class TableStateContainerServices
    {
        public Dictionary<string, TableState> State { get; set; } = new();
        public void CreateTableData(string key)
        {
            State.Add(key, new TableState()); 
        } 
        public class TableState
        {
            public string Need { get; set; }
            public string Status { get; set; }
            public int? CuratorId { get; set; }
            public int? Employee { get; set; }
            public DateRange DateRange { get; set; }
            public int? ProjectId { get; set; }
            public string SearchString { get; set; }
            public bool IsFiltered { get; set; }
            public bool IsNavigation { get; set; }
            public int PageNumber { get; set; }
            public bool? IsComfirm { get; set; }
            public string Source { get; set; }
            public bool IsDeleted { get; set; }
            public string Progress { get; set; }
        }
    }
}