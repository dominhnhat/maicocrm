﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class DocumentNoteServices
    {

        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public DocumentNoteServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public async Task AddAsync(DocumentNote note)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Add(note);
            await context.SaveChangesAsync();
            context.Entry(note).Reference(c => c.EmployeeNavigation).Load();
        }
        public async Task<List<DocumentNote>> GetAllByDocumentIdAsync(int documentId)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.DocumentNotes
                .Include(c => c.EmployeeNavigation)
                .Where(c => c.DocumentId == documentId)
                .ToListAsync();
        }
        public async Task DeleteAsync(DocumentNote note)
        {
            using var context = _contextFactory.CreateDbContext();
            context.DocumentNotes.Remove(note);
            await context.SaveChangesAsync();
        }
        public async Task UpdateAsync(DocumentNote note)
        {
            using var context = _contextFactory.CreateDbContext();
            context.DocumentNotes.Update(note);
            await context.SaveChangesAsync();
        }
    }
}