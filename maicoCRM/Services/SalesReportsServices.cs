﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using maicoCRM.Models;
using maicoCRM.Data;

namespace maicoCRM.Services
{
    public class SalesReportsServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;

        public SalesReportsServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public async Task <List<SalesReport>> GetByMonth(DateTime month)
        {
            using var context = _contextFactory.CreateDbContext();

            return await context.SalesReports.Where(x => x.Month.Month == month.Month).ToListAsync();
        }
        public async Task<List<SalesReport>> GetDataAsync(DateTime month)
        {
            month = new DateTime(month.Year, month.Month, 1);

            DateTime start = month;
            var temp = month.AddMonths(1);
            DateTime end = new DateTime(temp.Year, temp.Month, 10);

            using var context = _contextFactory.CreateDbContext();
            return await context.SalesReports
                                .Include(c => c.EmployeeNavigation)
                                    .ThenInclude(c => c.OrderDetails
                                                        .Where(c => c.OrderNavigation.CreatedAt >= start && c.OrderNavigation.CreatedAt < end))
                                        .ThenInclude(c => c.OrderNavigation)
                                .Include(c => c.EmployeeNavigation)
                                    .ThenInclude(c => c.OfficeNavigation)
                                .AsSplitQuery()
                                .Where(c => c.Month == month)
                                .ToListAsync();
        }
        public async Task Update(List<SalesReport> rps)
        {
            using var context = _contextFactory.CreateDbContext();
            context.SalesReports.UpdateRange(rps);
            await context.SaveChangesAsync();
        }

        public async Task AddRangeAsync(List<SalesReport> rps)
        {
            using var context = _contextFactory.CreateDbContext();
            context.SalesReports.AddRange(rps);
            await context.SaveChangesAsync();
        }
        public async Task Add(SalesReport salesReport)
        {
            var employee = salesReport.EmployeeNavigation;
            salesReport.EmployeeNavigation = null;

            using var context = _contextFactory.CreateDbContext();
            context.SalesReports.Add(salesReport);
            await context.SaveChangesAsync();

            //context.Entry(employee).Collection(c => c.SalesReportsNavigation).Load();
        }
    }
}
