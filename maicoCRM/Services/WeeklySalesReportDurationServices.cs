﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using maicoCRM.Models;
using maicoCRM.Data;

namespace maicoCRM.Services
{
    public class WeeklySalesReportDurationServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;

        public WeeklySalesReportDurationServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }

        public async Task<List<WeeklySalesReportDuration>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            return await context.WeeklySalesReportDurations.ToListAsync();
        }

        public async Task<WeeklySalesReportDuration> GetLastDurationAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            var maxDurationId = context.WeeklySalesReportDurations.Max(x => x.Id);
            return await context.WeeklySalesReportDurations.Where(x => x.Id == maxDurationId)
                                                           .FirstOrDefaultAsync();
        }

        public async Task<List<WeeklySalesReportDuration>> GetDurationInCurrentMonth(int month)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.WeeklySalesReportDurations.Where(x => x.Month.Month == month).ToListAsync();
        }

        public async Task UpdateAsync(WeeklySalesReportDuration reportDuration)
        {
            using var context = _contextFactory.CreateDbContext();
            context.WeeklySalesReportDurations.Update(reportDuration);
            await context.SaveChangesAsync();
        }
        public async Task UpdateRangeAsync(List<WeeklySalesReportDuration> reportDurations)
        {
            using var context = _contextFactory.CreateDbContext();
            context.WeeklySalesReportDurations.UpdateRange(reportDurations);
            await context.SaveChangesAsync();
        }
        public async Task<int> AddAsync(WeeklySalesReportDuration reportDuration)
        {
            using var context = _contextFactory.CreateDbContext();
            context.WeeklySalesReportDurations.Add(reportDuration);
            await context.SaveChangesAsync();
            return reportDuration.Id;
        }
    }
}
