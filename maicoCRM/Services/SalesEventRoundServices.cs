﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class SalesEventRoundServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        readonly ConfigServices _configServices;
        public SalesEventRoundServices(IDbContextFactory<MaicoCRMContext> context, ConfigServices configServices)
        {
            _contextFactory = context;
            _configServices = configServices;
        }

        public async Task AddAsync(SalesEventRound round)
        {
            using var context = _contextFactory.CreateDbContext();
            context.SalesEventRounds.Add(round);
            await context.SaveChangesAsync();
        }
        public async Task UpdateAsync(SalesEventRound round)
        {
            using var context = _contextFactory.CreateDbContext();
            context.SalesEventRounds.Update(round);
            await context.SaveChangesAsync();
        }
        public async Task DeleteAsync(SalesEventRound round)
        {
            using var context = _contextFactory.CreateDbContext();
            context.SalesEventRounds.Remove(round);
            await context.SaveChangesAsync();
        }
        public async Task<List<SalesEventRound>> GetByMonthYear(DateTime date)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.SalesEventRounds
                .Where(x => x.StartDate.Month == date.Month && x.StartDate.Year == date.Year)
                .ToListAsync();
        }
        public async Task<SalesEventRound> GetByMonthAsync(DateTime month)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.SalesEventRounds.Where(x => x.StartDate.Month == month.Month && x.StartDate.Year == month.Year)
                                                 .FirstOrDefaultAsync();
        }
    }
}
