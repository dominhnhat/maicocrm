﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using MudBlazor;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class ProjectServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public ProjectServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public List<Project> GetAll(bool includeAreaNavigation = false)
        {
            using var context = _contextFactory.CreateDbContext();
            if (includeAreaNavigation) return context.Projects.Include(p => p.AreaNavigation).ToList();
            return context.Projects.ToList();
        }
        public async Task<List<Project>> GetAllAsync(bool includeAreaNavigation = false)
        {
            using var context = _contextFactory.CreateDbContext();
            if (includeAreaNavigation) return await context.Projects.Include(x => x.SheetMarketings)
                                         .Include(x => x.AreaNavigation)
                                         .ToListAsync();

            return await context.Projects.Include(x => x.SheetMarketings)
                                         .ToListAsync();
        }

        public async Task<List<Project>> GetByReportMarketing(DateRange dateRange)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Projects.Where(x => x.ProjectType.Equals("Chính"))
                                         .Include(x => x.SheetMarketings.Where(x => x.DateCreated.Date >= dateRange.Start.Value.Date &&
                                                        x.DateCreated.Date <= dateRange.End.Value.Date))
                                         .Include(x => x.AreaNavigation)
                                         .ToListAsync();
        }

        public Project GetById(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Projects.Where(c => c.Id == id).FirstOrDefault();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetName(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Projects.Where(x => x.Id == id).FirstOrDefault().Name;
        }
        public void Add(Project project)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Projects.Add(project);
            context.SaveChanges();
            context.Entry(project).Reference(p => p.AreaNavigation).Load();
        }
        public void Delete(Project project)
        {
            using var context = _contextFactory.CreateDbContext();
            try
            {
                context.Remove(project);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public int GetDocumentIdById(int Id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Projects.Where(c => c.Id == Id).FirstOrDefault().DocumentId;
        }
        public void Update(Project project)
        {
            using var context = _contextFactory.CreateDbContext();
            project.AreaId = project.AreaNavigation.Id;
            project.AreaNavigation = null;
            context.Projects.Update(project);
            context.SaveChanges();
            context.Entry(project).Reference(p => p.AreaNavigation).Load();
        }
    }
}
