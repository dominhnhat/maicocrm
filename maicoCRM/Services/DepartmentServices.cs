﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class DepartmentServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;

        public DepartmentServices(IDbContextFactory<MaicoCRMContext> dbContextFactory)
        {
            _contextFactory = dbContextFactory;
        }

        public async Task<List<Department>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Departments.ToListAsync();
        }
        public List<Department> GetAll()
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Departments.ToList();
        }
        public void Add (Department department)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Departments.Add(department);
            context.SaveChanges();
        }
        public void Update(Department department)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Departments.Update(department);
            context.SaveChanges();
        }
        public void Delete(Department department)
        {
            using var context = _contextFactory.CreateDbContext();
            Department _department = new Department() { Id = department.Id };
            context.Remove<Department>(_department);
            context.SaveChanges();
        }
    }
}
