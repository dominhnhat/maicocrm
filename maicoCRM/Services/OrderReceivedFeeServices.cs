﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using maicoCRM.Data;
using maicoCRM.Models;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace maicoCRM.Services
{
    public class OrderReceivedFeeServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public OrderReceivedFeeServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public List<OrderReceivedFee> GetByOrderId(int OrderId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.OrderReceivedFees.Where(x => x.OrderId == OrderId)
                .Include(x => x.OrderNavigation)
                .Include(x=>x.CollecterNavigation)
                .Include(x=>x.ReceiverNavigation)
                .Include(x=>x.CreaterNavigation)
                .ToList();
        }
        public void Add(OrderReceivedFee orderReceivedFee)
        {
            using var context = _contextFactory.CreateDbContext();

            context.OrderReceivedFees.Add(orderReceivedFee);
            context.SaveChanges();

            context.Entry(orderReceivedFee).Reference(r => r.CollecterNavigation).Load();
            context.Entry(orderReceivedFee).Reference(r => r.ReceiverNavigation).Load();
            context.Entry(orderReceivedFee).Reference(r => r.CreaterNavigation).Load();
            context.Entry(orderReceivedFee).Reference(r => r.OrderNavigation).Load();
        }

        public OrderReceivedFee Copy(OrderReceivedFee orderReceivedFee)
        {
            OrderReceivedFee tempOrderReceivedFee = new();

            if (orderReceivedFee == null)
            {
                return null;
            }

            tempOrderReceivedFee.CreatedAt = orderReceivedFee.CreatedAt;
            tempOrderReceivedFee.FeeImg = orderReceivedFee.FeeImg;
            tempOrderReceivedFee.OrderId = orderReceivedFee.OrderId;
            tempOrderReceivedFee.CreaterId = orderReceivedFee.CreaterId;
            tempOrderReceivedFee.CollecterId = orderReceivedFee.CollecterId;
            tempOrderReceivedFee.ReceivedFee = orderReceivedFee.ReceivedFee;
            tempOrderReceivedFee.CreaterNavigation = orderReceivedFee.CreaterNavigation;
            tempOrderReceivedFee.CollecterNavigation = orderReceivedFee.CollecterNavigation;
            tempOrderReceivedFee.ReceiverNavigation = orderReceivedFee.ReceiverNavigation;
            tempOrderReceivedFee.OrderNavigation = orderReceivedFee.OrderNavigation;

            return tempOrderReceivedFee;
        }
    }
}

