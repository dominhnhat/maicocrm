﻿using Microsoft.AspNetCore.Razor.Language;
using MudBlazor;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq; 
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks; 

namespace maicoCRM.Services
{
    public class ConfigServices
    { 
        public Config GetAll()
        {
            using (StreamReader file = File.OpenText("config.json"))
            {
                using (JsonTextReader reader = new(file))
                {
                    var json = (JObject)JToken.ReadFrom(reader);
                    try
                    {
                        return JsonConvert.DeserializeObject<Config>(json.ToString());
                    }
                    catch
                    {
                        var oldRelationship = json["Relationships"];
                        json.Property("Relationships").Remove();
                        Config config = JsonConvert.DeserializeObject<Config>(json.ToString());
                        config.Relationships = new();
                        for (int i = 0; i < oldRelationship.Count(); i++)
                        {
                            Relationship relationship = new()
                            {
                                Name = oldRelationship[i].ToString(),
                                Color = "#19B33D"
                            };
                            config.Relationships.Add(relationship);
                        }
                        string jsonData = JsonConvert.SerializeObject(config);
                        File.WriteAllText("config.json", jsonData);
                    }
                    return JsonConvert.DeserializeObject<Config>(json.ToString());
                }
            } 
        }
        public async Task<Config> GetAllAsync()
        { 
            using (StreamReader file = File.OpenText("config.json"))
            {
                using (JsonTextReader reader = new(file))
                {
                    var json = (JObject)JToken.ReadFrom(reader);
                    try
                    {
                        return JsonConvert.DeserializeObject<Config>(json.ToString());
                    }
                    catch
                    {
                        var oldRelationship = json["Relationships"];
                        json.Property("Relationships").Remove();
                        Config config = JsonConvert.DeserializeObject<Config>(json.ToString());
                        config.Relationships = new();
                        for (int i = 0; i < oldRelationship.Count(); i++)
                        {
                            Relationship relationship = new()
                            {
                                Name = oldRelationship[i].ToString(),
                                Color = "#19B33D"
                            };
                            config.Relationships.Add(relationship);
                        }
                        string jsonData = JsonConvert.SerializeObject(config);
                        await File.WriteAllTextAsync("config.json", jsonData);
                    }
                    return JsonConvert.DeserializeObject<Config>(json.ToString());
                }
            }
        }
        public async Task WriteToDiskAsync(Config config, string PropName, List<string> ListData, List<string> ListColor = null)
        {
            Console.WriteLine(config.Relationships[0].GetType());
            if (ListData != null && ListColor == null)
            {
                    config.SetValueByPropName(PropName, ListData);                
            }
            else if(ListColor != null)
            {
                config.Relationships.Clear(); 
                for(int i = 0; i < ListData.Count; i++)
                {
                    Relationship temp = new()
                    {
                        Name = ListData[i],
                        Color = ListColor[i]
                    };
                    config.Relationships.Add(temp);
                } 
            }
            string jsonData = JsonConvert.SerializeObject(config);
            await File.WriteAllTextAsync("config.json", jsonData);
            if (PropName == "DocumentTypes")
            {
                ListData.Remove("Tài Liệu Dự Án");
            }
        } 
    }  
    public class Config
    {
        public List<Relationship> Relationships { get; set; }
        public List<string> Sources { get; set; } 
        public List<string> Needs { get; set; }
        public List<string> JobTypes { get; set; }
        public List<string> ScheduleTypes { get; set; }
        public List<string> DocumentTypes { get; set; }
        public string NewFeed { get; set; }
        public List<string> MarketingSources { get; set; }
        public List<string> MarketingNeeds { get; set; }
        public List<string> ListProgress { get; set; }
        public void SetValueByPropName(string propName, object data)
        {
            Type type = this.GetType();
            PropertyInfo propertyInfo = type.GetProperty(propName);
            propertyInfo.SetValue(this, data, null);
        }
    }

    public class Relationship
    {
        public string Name { get; set; }
        public string Color { get; set; }
    }
}
