using maicoCRM.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace maicoCRM.Services
{
    public class SheetBookRealtimeServices
    {
        private static readonly Mutex mutex = new();
        public void Update(SheetMarketing sheet)
        {
            mutex.WaitOne();
            UpdateEvent?.Invoke(sheet);
            mutex.ReleaseMutex();
        }
        public event Func<SheetMarketing, Task> UpdateEvent;
    }
}