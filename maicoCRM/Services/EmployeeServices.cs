﻿using LinqKit;
using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MudBlazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class EmployeeServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;

        public EmployeeServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }

        public async Task AddAsync(Employee emp)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Employees.Add(emp);
            await context.SaveChangesAsync();
        }
        public async Task SaveChangesAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            await context.SaveChangesAsync();
        }
        public List<Employee> GetAll()
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Employees.Include(c => c.OfficeNavigation).Where(c => c.OfficeId > 0).ToList();
        }
        public string GetName(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Employees.Where(x => x.Id == id).FirstOrDefault().Name;
        }

        public async Task<List<Employee>> GetAllAsync(bool includeDepartmentNav = false)
        {
            using var context = _contextFactory.CreateDbContext();
            if(includeDepartmentNav) return await context.Employees
                                        .Include(x => x.DepartmentNavigation)
                                        .ToListAsync();
            return await context.Employees
                                .ToListAsync();
        }
        public Employee GetEmployeeById(int Id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Employees
                .Include(x => x.DepartmentNavigation)
                .Include(x => x.OfficeNavigation)
                .Where(x => x.Id == Id)
                .FirstOrDefault();
        }
        public async Task<List<Employee>> GetSalesByEmployee(DateRange dateRange, bool includeLockEmployee = false)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Employees
                                .Where(x => x.DepartmentNavigation.Name.Equals("Sales"))
                                .Where(e => !includeLockEmployee && (e.IsBlocked != true || e.OrderDetails
                                                                                                .Where(c => c.OrderNavigation.CreatedAt.Date >= dateRange.Start.Value.Date
                                                                                                            && c.OrderNavigation.CreatedAt.Date <= dateRange.End.Value.Date)
                                                                                                .Sum(c => c.OrderJobTypes
                                                                                                            .Sum(c => c.PercentContribute) * c.OrderNavigation.BrokerageFee / 100) > 0))
                                .Include(c => c.OfficeNavigation)
                                .Include(x => x.OrderDetails.Where(x => x.OrderNavigation.IsConfirmed == true 
                                                                        && x.OrderNavigation.IsActive == true
                                                                        && x.OrderNavigation.CreatedAt.Date >= dateRange.Start.Value.Date
                                                                        && x.OrderNavigation.CreatedAt.Date <= dateRange.End.Value.Date))
                                    .ThenInclude(x => x.OrderNavigation)
                                .Include(x => x.OrderDetails.Where(x => x.OrderNavigation.IsConfirmed == true 
                                                                        && x.OrderNavigation.IsActive == true
                                                                        && x.OrderNavigation.CreatedAt.Date >= dateRange.Start.Value.Date
                                                                        && x.OrderNavigation.CreatedAt.Date <= dateRange.End.Value.Date))
                                    .ThenInclude(x => x.OrderJobTypes)
                                .ToListAsync();
        }
        public List<Employee> GetSalesByDate(DateTime date)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Employees
                                .Where(x => x.DepartmentNavigation.Name.Equals("Sales"))
                                .Include(c => c.OfficeNavigation)
                                .Include(x => x.OrderDetails.Where(x => x.OrderNavigation.IsConfirmed == true 
                                                                        && x.OrderNavigation.IsActive == true
                                                                        && x.OrderNavigation.CreatedAt.Date == date.Date))
                                    .ThenInclude(x => x.OrderNavigation)
                                .Include(x => x.OrderDetails.Where(x => x.OrderNavigation.IsConfirmed == true 
                                                                        && x.OrderNavigation.IsActive == true
                                                                        && x.OrderNavigation.CreatedAt.Date == date.Date))
                                    .ThenInclude(x => x.OrderJobTypes)
                                .ToList();
        }
        public Employee GetCurrentEmployeeByIdIncludeCustom(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Employees
                        .Include(c => c.SalesReportsNavigation)
                        .Include(c => c.WeeklySalesReports)
                            .ThenInclude(c => c.DurationNavigation)
                        .FirstOrDefault(f => f.Id == id);
        }
        public async Task UpdateAsync(Employee employee)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Employees.Update(employee);
            await context.SaveChangesAsync();
        }
        public async Task UpdateRangeAsync(List<Employee> employees)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Employees.UpdateRange(employees);
            await context.SaveChangesAsync();
        }
        public async Task<Employee> GetByEmailAsync(string email)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Employees.FirstOrDefaultAsync(x => x.Email == email);
        }
        public async Task<List<Employee>> GetSalesReportData(DateTime month)
        {
            month = new DateTime(month.Year, month.Month, 1);

            DateTime start = month;
            var temp = month.AddMonths(1);
            DateTime end = new DateTime(temp.Year, temp.Month, 10);

            using var context = _contextFactory.CreateDbContext();
            return await context.Employees
                                .Include(c => c.SalesReportsNavigation.Where(c => c.Month == month.AddDays(4)))
                                .Include(c => c.WeeklySalesReports
                                                .Where(c => c.DurationNavigation.Month == month.AddDays(4))
                                                .OrderBy(c => c.DurationNavigation.DateStart))
                                .ThenInclude(c => c.DurationNavigation)
                                .Include(c => c.OfficeNavigation)
                                .Include(c => c.DepartmentNavigation)
                                .Include(c => c.OrderDetails
                                                        .Where(c => c.OrderNavigation.CreatedAt >= start && c.OrderNavigation.CreatedAt < end &&
                                                        c.OrderNavigation.IsConfirmed == true && c.OrderNavigation.IsActive == true))
                                        .ThenInclude(c => c.OrderNavigation)
                                .Include(c => c.OrderDetails
                                                .Where(c => c.OrderNavigation.CreatedAt >= start && c.OrderNavigation.CreatedAt < end &&
                                                c.OrderNavigation.IsConfirmed == true && c.OrderNavigation.IsActive == true))
                                        .ThenInclude(c => c.OrderJobTypes)
                                .Where(c => c.OfficeId > 0 && (c.IsBlocked == false || c.OrderDetails.Any(c => c.OrderNavigation.CreatedAt >= start.AddDays(4)
                              && c.OrderNavigation.CreatedAt < end.AddDays(-5) && c.OrderNavigation.IsConfirmed == true && c.OrderNavigation.IsActive == true)) && c.DepartmentNavigation.Name != "Other")
                                .AsSplitQuery()
                                .ToListAsync();
        }
        public async Task<List<Employee>> GetWorkDashboardData(DateTime start, DateTime end, string office)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Employees
                .Where(x => x.OfficeNavigation.Name.Contains(office) && x.OfficeId > 1 && x.IsBlocked == false &&
                            x.DepartmentNavigation.Name.Equals("Sales"))
                .Include(x => x.OfficeNavigation)
                .Include(x => x.ExploitedCustomerInformations
                    .Where(x =>
                        x.CreatedAt.Date >= start.Date &&
                        x.CreatedAt.Date <= end.Date))
                .Include(x => x.OrderDetails
                    .Where(x =>
                        x.OrderNavigation.CreatedAt.Date >= start.Date &&
                        x.OrderNavigation.CreatedAt.Date <= end.Date &&
                        x.OrderNavigation.IsConfirmed == true &&
                        x.OrderNavigation.IsActive == true))
                    .ThenInclude(x => x.OrderNavigation)
                .Include(x => x.OrderDetails
                    .Where(x =>
                        x.OrderNavigation.CreatedAt.Date >= start.Date &&
                        x.OrderNavigation.CreatedAt.Date <= end.Date &&
                        x.OrderNavigation.IsConfirmed == true && 
                        x.OrderNavigation.IsActive == true))
                    .ThenInclude(x => x.OrderJobTypes)
                .AsSplitQuery()
                .ToListAsync();
        }
    }
}