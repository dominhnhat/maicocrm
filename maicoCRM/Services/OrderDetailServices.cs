﻿using maicoCRM.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using maicoCRM.Models;

namespace maicoCRM.Services
{
    public class OrderDetailServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public OrderDetailServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public void ConfirmOrder(int orderId, int employeeId)
        {
            using var context = _contextFactory.CreateDbContext();
            var findOrderDetail = context.OrderDetails.Where(x => x.OrderId == orderId && x.EmployeeId == employeeId).FirstOrDefault();
            findOrderDetail.IsConfirmed = true;
            context.OrderDetails.Update(findOrderDetail);
            context.SaveChanges();
        }
        public void RemoveAllByOrderId(int orderId)
        {
            using var context = _contextFactory.CreateDbContext();
            var findOrderDetail = context.OrderDetails.Where(x => x.OrderId == orderId).ToList();
            context.OrderDetails.RemoveRange(findOrderDetail);
            context.SaveChanges();
        }
        public void AddRange(List<OrderDetail> listOrderDetail)
        {
            using var context = _contextFactory.CreateDbContext();
            context.OrderDetails.AddRange(listOrderDetail);
            context.SaveChanges();
        }
        public List<OrderDetail> GetByOrderId(int OrderId)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.OrderDetails.Where(x => x.OrderId == OrderId)
                .Include(x => x.EmployeeNavigation)
                .Include(x=>x.OrderJobTypes)
                .ToList();
        }

        public async Task<List<OrderDetail>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.OrderDetails.ToListAsync();
        }
    }
}
