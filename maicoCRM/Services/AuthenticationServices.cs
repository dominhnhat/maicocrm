﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class AuthenticationServices
    {
        readonly AuthenticationStateProvider _authenticationStateProvider;
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        int _currentEmployeeId;
        public AuthenticationServices(IDbContextFactory<MaicoCRMContext> contextFactory, AuthenticationStateProvider authenticationStateProvider)
        {
            _contextFactory = contextFactory;
            _authenticationStateProvider = authenticationStateProvider;
        }
        string GetCurrentUserEmail()
        {
            var authState = _authenticationStateProvider.GetAuthenticationStateAsync();
            return authState.Result.User.FindFirst(ClaimTypes.Email)?.Value;
        }
        public Employee GetCurrentEmployee()
        {
            var email = GetCurrentUserEmail();
            using var context = _contextFactory.CreateDbContext();
            return context.Employees.Include(x => x.DepartmentNavigation)
                                    .FirstOrDefault(f => f.Email == email);
        }
        public async Task<int> GetCurrentEmployeeId()
        {
            var email = GetCurrentUserEmail();
            if (_currentEmployeeId == 0)
            {
                using var context = _contextFactory.CreateDbContext();
                var temp = await context.Employees.FirstAsync(f => f.Email == email);
                if (temp != null)
                {
                    _currentEmployeeId = temp.Id;
                    return _currentEmployeeId;
                }
                else
                    throw new Exception("Employee Not Found");
            }
            else
            {
                return _currentEmployeeId;
            }
        }
    }
}
