﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using LinqKit;

namespace maicoCRM.Services
{
    public class LegalRecordServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        ExpressionStarter<LegalRecord> _filterCondition;
        string _progressCondition;
        public LegalRecordServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public async Task<(int, List<LegalRecord>)> FilterByPage(int page, string progress, string searchString, int? projectId, int? curatorId)
        {
            _progressCondition = progress;
            _filterCondition = PredicateBuilder.New<LegalRecord>();

            if (String.IsNullOrEmpty(progress) &&
                String.IsNullOrEmpty(searchString) &&
                projectId == null && curatorId == null)
            {
                _filterCondition.And(c => true);
            }
            else
            {
                if (!String.IsNullOrEmpty(progress))
                {
                    _filterCondition.And(c => EF.Functions.ILike(c.Progress, _progressCondition));
                }
                if (!String.IsNullOrEmpty(searchString))
                {
                    var tempFilter = PredicateBuilder.New<LegalRecord>();
                    var phoneNumber = searchString;
                    searchString = searchString?.Insert(0, " ");
                    searchString += " ";
                    searchString = searchString?.Replace(" ", "%");

                    tempFilter.Or(c => EF.Functions.JsonExists(c.OrderNavigation.CustomerNavigation.PhoneNumber, $"{phoneNumber}"));
                    tempFilter.Or(c => EF.Functions.ILike(c.OrderNavigation.ApartmentCode, searchString));
                    tempFilter.Or(c => c.ExploitedLegalInformations.Any(e => EF.Functions.ILike(e.Comment, searchString)));
                    _filterCondition.And(tempFilter);
                }
                if (projectId != null) _filterCondition.And(c => c.OrderNavigation.ProjectNavigation.Id == projectId);
                if (curatorId != null) _filterCondition.And(c => c.OrderNavigation.CustomerNavigation.CuratorNavigation.Id == curatorId);
            }

            using var context = _contextFactory.CreateDbContext();

            var result = context.LegalRecords
                                            .Include(c => c.OrderNavigation)
                                                .ThenInclude(c => c.ProjectNavigation)
                                            .Include(c => c.OrderNavigation)
                                                .ThenInclude(c => c.CustomerNavigation)
                                            .Where(_filterCondition)
                                            .OrderByDescending(c => c.CreatedAt)
                                            .Skip((page / 10) * 100)
                                            .Take(100).ToQueryString();
            return (context.LegalRecords.Where(_filterCondition).Count(), await context.LegalRecords
                                                                                .Include(c => c.OrderNavigation)
                                                                                    .ThenInclude(c => c.ProjectNavigation)
                                                                                .Include(c => c.OrderNavigation)
                                                                                    .ThenInclude(c => c.CustomerNavigation)
                                                                                .Where(_filterCondition)
                                                                                .OrderByDescending(c => c.CreatedAt)
                                                                                .Skip((page / 10) * 100)
                                                                                .Take(100)
                                                                                .ToListAsync());
        }
        public void GetProgressCount(List<string> listProgress, ref Dictionary<string, int> progressCount)
        {
            using var context = _contextFactory.CreateDbContext();
            foreach (var item in listProgress)
            {
                _progressCondition = item;
                progressCount[item] = context.LegalRecords.Where(c => EF.Functions.ILike(c.Progress, _progressCondition)).Where(_filterCondition).Count();
            }
        }

        public async Task<LegalRecord> GetByIdAsync(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.LegalRecords
                                .Include(l => l.OrderNavigation.ProjectNavigation)
                                .Include(l => l.OrderNavigation.CustomerNavigation.ProjectNavigation)
                                .Include(l => l.OrderNavigation.CustomerNavigation.CreatorNavigation)
                                .Include(l => l.OrderNavigation.CustomerNavigation.CuratorNavigation)
                                .FirstOrDefaultAsync(l => l.Id == id);
        }
        public async Task AddAsync(LegalRecord legalRecord)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                context.LegalRecords.Add(legalRecord);
                context.SaveChanges();
                await context.Entry(legalRecord).Reference(r => r.OrderNavigation).LoadAsync();
            }
        }
        public void Update(LegalRecord legalRecord)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Entry(legalRecord).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void ChangeApartmentCode(LegalRecord legalRecord)
        {
            using var context = _contextFactory.CreateDbContext();
            var order = legalRecord.OrderNavigation;
            context.Entry(order).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}
