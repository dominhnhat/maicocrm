﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;

namespace maicoCRM.Services
{
    public class OfficesServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public OfficesServices(IDbContextFactory<MaicoCRMContext> contextFactory)
        {
            _contextFactory = contextFactory;
        }
        public async Task<List<Office>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Offices.ToListAsync();
        }
        public async Task AddAsync(Office office)
        {
            using var context = _contextFactory.CreateDbContext();
            await context.Offices.AddAsync(office);
        }
    }
}
