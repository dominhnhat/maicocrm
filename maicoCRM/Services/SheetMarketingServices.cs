﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using MudBlazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class SheetMarketingServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        readonly ConfigServices _configServices;
        public SheetMarketingServices(IDbContextFactory<MaicoCRMContext> context, ConfigServices configServices)
        {
            _contextFactory = context;
            _configServices = configServices;
        }

        public List<SheetMarketing> GetByPhone(string phone)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.SheetMarketings.Where(x => x.PhoneNumber == phone).ToList();
        }

        public void UpdateRange(List<SheetMarketing> sheets)
        {
            using var context = _contextFactory.CreateDbContext();
            context.SheetMarketings.UpdateRange(sheets);
            context.SaveChanges();
        }
        public void DeleteRange(List<SheetMarketing> sheets)
        {
            using var context = _contextFactory.CreateDbContext();
            context.SheetMarketings.RemoveRange(sheets);
            context.SaveChanges();
        }

        public async Task<List<SheetMarketing>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.SheetMarketings
                .OrderByDescending(x => x.DateCreated)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.CustomerNavigation)
                .Include(x => x.Bookers)
                .Include(x => x.CreaterNavigation)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<SheetMarketing>> GetByDateAsync(DateTime date)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.SheetMarketings
                .Where(x => x.DateCreated.Date == date.Date)
                .OrderBy(x => x.Bookers.Count)
                    .ThenByDescending(x => x.DateCreated)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.CustomerNavigation)
                .Include(x => x.Bookers)
                .Include(x => x.CreaterNavigation)
                .Include(x => x.Bookers)
                .AsNoTracking()
                .ToListAsync();
        }
        public async Task<List<SheetMarketing>> GetByDateRangeAsync(DateRange dateRange)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.SheetMarketings
                .Where(x => x.DateCreated.Date >= dateRange.Start.Value.Date &&
                            x.DateCreated.Date <= dateRange.End.Value.Date)
                .ToListAsync();
        }
        public List<SheetMarketing> GetByDateRange(DateRange dateRange)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.SheetMarketings
                .Where(x => x.DateCreated.Date >= dateRange.Start.Value.Date &&
                            x.DateCreated.Date <= dateRange.End.Value.Date)
                .ToList();
        }

        public SheetMarketing GetSheetById(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.SheetMarketings
                .Include(s => s.Bookers)
                .Include(s => s.CustomerNavigation)
                .Include(s => s.ProjectNavigation)
                .Include(s => s.CreaterNavigation)
                .FirstOrDefault(x => x.Id == id);
        }

        public void Update(SheetMarketing sheet)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Update<SheetMarketing>(sheet);
            context.SaveChanges();
            context.Entry(sheet).Reference(x => x.ProjectNavigation).Load();
            context.Entry(sheet).Collection(x => x.Bookers).Load();
        }
        public void AddOrUpdate(SheetMarketing sheet)
        {
            using var context = _contextFactory.CreateDbContext();
            //var entity = context.SheetMarketings.Find(sheet.Id);
            //context.Entry(entity).CurrentValues.SetValues(sheet);
            if(sheet.Id > 0)
            {
                context.SheetMarketings.Update(sheet);
            }
            else
            {
                context.SheetMarketings.Add(sheet);
            }
            
            context.SaveChanges();
        }
        public void AddBooker(SheetMarketing sheet, Employee booker)
        {
            using var context = _contextFactory.CreateDbContext();
            var temp = new BookerSheet()
            {
                BookerId = booker.Id,
                SheetMarketingId = sheet.Id
            };
            context.Add(temp);
            context.SaveChanges();
        }

        public void AddCustomerInSheet(SheetMarketing sheet, Customer customer)
        {
            using var context = _contextFactory.CreateDbContext();

            sheet.CustomerId = customer.Id;

            context.SaveChanges();
        }

        public void Delete(SheetMarketing sheetMarketing)
        {
            using var context = _contextFactory.CreateDbContext();

            SheetMarketing sheet = new SheetMarketing() { Id = sheetMarketing.Id };

            context.Remove<SheetMarketing>(sheet);

            context.SaveChanges();
        }

        public void Add(SheetMarketing sheet)
        {
            using var context = _contextFactory.CreateDbContext();

            context.SheetMarketings.Add(sheet);

            context.SaveChanges();
            context.Entry(sheet).Reference(c => c.ProjectNavigation).Load();
        }
    }
}
