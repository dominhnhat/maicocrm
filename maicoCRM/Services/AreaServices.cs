﻿using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;

using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Services
{
    public class AreaServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        public AreaServices(IDbContextFactory<MaicoCRMContext> context)
        {
            _contextFactory = context;
        }
        public async Task<List<Area>> GetAllAsync()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Areas.Include(x => x.Projects)
                                         .ToListAsync();
        }
        public List<Area> GetAll()
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Areas.Include(p => p.Projects).ToList();
        }
        public Area GetById(int id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Areas.Where(a => a.Id == id).Include(p => p.Projects).FirstOrDefault();
        }
        public void Add(Area area)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Areas.Add(area);
            context.SaveChanges();
        }
        public void Update(Area area)
        {
            using var context = _contextFactory.CreateDbContext();
            context.Areas.Update(area);
            context.SaveChanges();
        }
        public void Delete(Area area)
        {
            using var context = _contextFactory.CreateDbContext();
            Area _area = new Area() { Id = area.Id };
            context.Remove<Area>(_area);
            context.SaveChanges();
        }
    }
}
