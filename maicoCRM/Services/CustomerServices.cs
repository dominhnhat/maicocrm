﻿using LinqKit;
using maicoCRM.Data;
using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Diagnostics;
using Google.Apis.Util;
using maicoCRM.Services;

namespace maicoCRM.Services
{
    public class CustomerServices
    {
        readonly IDbContextFactory<MaicoCRMContext> _contextFactory;
        readonly ConfigServices _configServices;
        private ExpressionStarter<Customer> _filterCondition;
        string _statusCondition;

        public CustomerServices(IDbContextFactory<MaicoCRMContext> context, ConfigServices services)
        {
            _configServices = services;
            _contextFactory = context;
            _filterCondition = PredicateBuilder.New<Customer>();
            _filterCondition.DefaultExpression = c => true;
        }

        public async Task<List<Customer>> GetAll()
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Customers
                .Include(c => c.ProjectNavigation)
                .Include(c => c.CuratorNavigation)
                .OrderByDescending(c => c.CreatedAt)
                .ToListAsync();
        }

        public Customer GetMax()
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Customers.OrderBy(x => x.Id)
                                    .Where(c => c.IsDelete == false || c.IsDelete == null)
                                    .Last();
        }
        public async Task<List<Customer>> GetByPage(int currentPage)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Customers
               .Include(c => c.ProjectNavigation)
               .Include(c => c.CuratorNavigation)
               .OrderByDescending(c => c.CreatedAt)
               .Skip(currentPage * 100)
               .Take(100)
               .Where(c => c.IsDelete == false || c.IsDelete == null)
               .ToListAsync();
        }
        public void Add(Customer customer)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Customers.Add(customer);
                context.SaveChanges();
                context.Entry(customer).Reference(r => r.CuratorNavigation).Load();
                context.Entry(customer).Reference(r => r.ProjectNavigation).Load();
            }
        }
        public async Task AddAsync(Customer customer)
        {
            using (var context = _contextFactory.CreateDbContext())
            {
                context.Customers.Add(customer);
                await context.SaveChangesAsync();
                context.Entry(customer).Reference(r => r.CuratorNavigation).Load();
                context.Entry(customer).Reference(r => r.ProjectNavigation).Load();
            }
        }
        public Customer GetCustomerById(int Id)
        {
            using var context = _contextFactory.CreateDbContext();
            return context.Customers
                .Include(x => x.CuratorNavigation)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.CreatorNavigation)
                .Where(x => x.Id == Id)
                .Where(c => c.IsDelete == false || c.IsDelete == null)
                .FirstOrDefault();
        }
        public async Task<Customer> GetCustomerByIdAsync(int Id)
        {
            using var context = _contextFactory.CreateDbContext();
            return await context.Customers
                .Include(x => x.CuratorNavigation)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.CreatorNavigation)
                .Where(x => x.Id == Id)
                .Where(c => c.IsDelete == false || c.IsDelete == null)
                .FirstOrDefaultAsync();
        }
        public Customer GetCustomerByPhone(string phone)
        {
            using var context = _contextFactory.CreateDbContext();
            //var query = context.Customers
            //    .Include(x => x.CreatorNavigation)
            //    .Include(x => x.ProjectNavigation)
            //    .Include(x => x.CuratorNavigation)
            //    .Where(x => EF.Functions.JsonExists(x.PhoneNumber,$"{phone}"))
            //    .ToQueryString();
            //System.Diagnostics.Debug.WriteLine(query);
            return context.Customers
                .Include(x => x.CreatorNavigation)
                .Include(x => x.ProjectNavigation)
                .Include(x => x.CuratorNavigation)
                .Include(x => x.SheetMarketings)
                .Where(x => EF.Functions.JsonExists(x.PhoneNumber, $"{phone}")).FirstOrDefault();
        }
        public void Update(Customer customer)
        {
            using var context = _contextFactory.CreateDbContext();
            //var currentVal = context.Customers.Find(customer.Id);
            //context.Entry(currentVal).CurrentValues.SetValues(customer);
            context.Entry(customer).State = EntityState.Modified;
            context.SaveChanges();
        }
        public async Task<List<Customer>> Filter(string need, string status, int? curatorId, int? projectId, string searchString)
        {
            _statusCondition = status;
            _filterCondition = PredicateBuilder.New<Customer>();

            if (String.IsNullOrEmpty(need) &&
                String.IsNullOrEmpty(status) &&
                String.IsNullOrEmpty(searchString) &&
                (curatorId == null || curatorId == 0) &&
                (projectId == null || projectId == 0))
            {
                _filterCondition.And(c => true);
            }
            else
            {
                if (!String.IsNullOrEmpty(need)) _filterCondition.And(c => c.Need == need);
                if (!String.IsNullOrEmpty(status)) _filterCondition.And(c => EF.Functions.ILike(c.Status, _statusCondition));
                if (!String.IsNullOrEmpty(searchString))
                {
                    var tempFilter = PredicateBuilder.New<Customer>();
                    var phoneNumber = searchString;
                    searchString = searchString?.Insert(0, " ");
                    searchString += " ";
                    searchString = searchString?.Replace(" ", "%");

                    tempFilter.Or(c => EF.Functions.JsonExists(c.PhoneNumber, $"{phoneNumber}"));
                    tempFilter.Or(c => EF.Functions.ILike(c.Description, searchString));
                    tempFilter.Or(c => EF.Functions.ILike(c.Name, searchString));
                    tempFilter.Or(c => c.ExploitedCustomerInformations.Any(e => EF.Functions.ILike(e.Comment, searchString)));
                    _filterCondition.And(tempFilter);
                }
                if (curatorId != null && curatorId != 0) _filterCondition.And(c => c.CuratorId == curatorId);
                if (projectId != null && projectId != 0) _filterCondition.And(c => c.ProjectInterestedId == projectId);
            }
            using var context = _contextFactory.CreateDbContext();
            return await context.Customers.Include(c => c.CuratorNavigation)
                                     .Include(c => c.ProjectNavigation)
                                     .Where(_filterCondition)
                                     .OrderByDescending(c => c.CreatedAt)
                                     .Where(c => c.IsDelete == false || c.IsDelete == null)
                                     .ToListAsync();
        }

        public void GetStatusCount(List<Relationship> customerRelation, ref Dictionary<Relationship, int> statusCount, bool isDelete)
        {
            using var context = _contextFactory.CreateDbContext();
            foreach (var item in customerRelation)
            {
                _statusCondition = item.Name;
                statusCount[item] = context.Customers.IgnoreQueryFilters()
                                                     .Where(c => EF.Functions
                                                     .ILike(c.Status, _statusCondition))
                                                     .Where(_filterCondition)
                                                     .Where(c => c.IsDelete == isDelete)
                                                     .Count();
            }
        }
        public async Task<(int, List<Customer>)> FilterByPage(int page, string need, string status, int? curatorId, int? projectId, string searchString, List<string> BlockedCustomerStatus, bool? isDeleted = null)
        {
            _statusCondition = status;
            _filterCondition = PredicateBuilder.New<Customer>();
            using var context = _contextFactory.CreateDbContext();

            if (BlockedCustomerStatus != null && String.IsNullOrEmpty(status))
            {
                _filterCondition.And(c => BlockedCustomerStatus.All(d => d != c.Status));
            }
            if (String.IsNullOrEmpty(need) &&
                String.IsNullOrEmpty(status) &&
                String.IsNullOrEmpty(searchString) &&
                (curatorId == null || curatorId == 0) &&
                (projectId == null || projectId == 0) &&
                 isDeleted == null)
            {
                _filterCondition.And(c => true);
            }
            else
            {
                if (!String.IsNullOrEmpty(need)) _filterCondition.And(c => c.Need == need);
                if (!String.IsNullOrEmpty(status))
                {
                    _filterCondition.And(c => EF.Functions.ILike(c.Status, _statusCondition));
                }
                if (!String.IsNullOrEmpty(searchString))
                {
                    var tempFilter = PredicateBuilder.New<Customer>();
                    var phoneNumber = searchString;
                    searchString = searchString?.Insert(0, " ");
                    searchString += " ";
                    searchString = searchString?.Replace(" ", "%");

                    tempFilter.Or(c => EF.Functions.JsonExists(c.PhoneNumber, $"{phoneNumber}"));
                    tempFilter.Or(c => EF.Functions.ILike(c.Description, searchString));
                    tempFilter.Or(c => EF.Functions.ILike(c.Name, searchString));
                    tempFilter.Or(c => c.ExploitedCustomerInformations.Any(e => EF.Functions.ILike(e.Comment, searchString)));
                    _filterCondition.And(tempFilter);
                }
                if (curatorId != null && curatorId != 0) _filterCondition.And(c => c.CuratorId == curatorId);
                if (projectId != null && projectId != 0) _filterCondition.And(c => c.ProjectInterestedId == projectId);
                if (isDeleted != null)
                {
                    _filterCondition.And(x => x.IsDelete == isDeleted);
                }
            }
            return (context.Customers.IgnoreQueryFilters().Where(_filterCondition).Count(), await context.Customers.IgnoreQueryFilters()
                                     .Include(c => c.ProjectNavigation)
                                     .Include(c => c.CuratorNavigation)
                                     .Where(_filterCondition)
                                     .OrderByDescending(c => c.CreatedAt)
                                     .Skip((page / 10) * 100)
                                     .Take(100)
                                     .ToListAsync());
        }
       
        public void StateChangeIsDeleted(Customer customer, bool state)
        {
            using var context = _contextFactory.CreateDbContext();
            customer.IsDelete = state;
            context.Entry(customer).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}
