﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace maicoCRM.Models
{
    public class Project
    {
        public Project()
        {
            Customers = new HashSet<Customer>();
            ScheduleCustomers = new HashSet<ScheduleCustomer>();
            Orders = new HashSet<Order>();
            SheetMarketings = new HashSet<SheetMarketing>();
        }
        [Column("id")]
        public int Id { get; set; }

        [MaxLength(100)]
        [Column("name")]
        public string Name { get; set; }
        [Column("project_type")]
        public string ProjectType { get; set; }
        [Column("document_id")]
        public int DocumentId { get; set; }

        [Column("area_id")]
        public int AreaId { get; set; }
        public virtual Area AreaNavigation { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<ScheduleCustomer> ScheduleCustomers { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<SheetMarketing> SheetMarketings { get; set; }
        public virtual Document DocumentNavigation { get; set; }

    }
    public enum ProjectType
    {
        Main,
        Other
    }
}
