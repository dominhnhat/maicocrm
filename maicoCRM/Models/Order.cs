﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace maicoCRM.Models
{
    public class Order
    {
        public Order()
        {
            OrderDetails = new List<OrderDetail>();
            OrderReceivedFees = new List<OrderReceivedFee>();
        }
        [Column("id")]
        public int Id { get; set; }

        [Column("apartment_code")]
        public string ApartmentCode { get; set; }

        [Column("brokerage_fee")]
        public double BrokerageFee { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("contract_type")]
        [MaxLength(20)]
        public string ContractType { get; set; } //Loại hợp đồng

        [Column("creator_id")]
        public int CreatorId { get; set; } //Id người tạo đơn hàng  

        [Column("customer_id")]
        public int CustomerId { get; set; }

        [Column("project_id")]
        public int ProjectId { get; set; }

        [Column("is_confirmed")]
        public bool IsConfirmed { get; set; } = false;

        [Column("is_active")]
        public bool? IsActive { get; set; }//để set được giá trị mặc định của bool là true thì phải drop notnull, điều này không hoàn hảo nhưng hoàn toàn ko ảnh hưởng đến project hay bất kì usecase nào!

        [Column("logs", TypeName = "jsonb")]
        public List<UpdateOrderLog> Logs { get; set; } = new();

        [Column("order_image")]
        public string OrderImg { get; set; }

        [Column("legal_reports_fee")]
        public double LegalReportsFee { get; set; }
        [Column("is_investment_order")]
        public bool IsInvestmentOrder { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Employee EmployeeNavigation { get; set; }
        public virtual Project ProjectNavigation { get; set; }
        public virtual LegalRecord LegalRecord { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; }
        public virtual List<OrderComment> OrderComments { get; set; }
        public virtual List<OrderReceivedFee> OrderReceivedFees { get; set; }
    }

    public class UpdateOrderLog
    {
        public int UpdaterId { get; set; }
        public string Name { get; set; }
        public DateTime Time { get; set; }
        public string Content { get; set; }
    }

    public class OrderComment
    {
        [Column("employee_id")]
        public int EmployeeId { get; set; }
        [Column("order_id")]
        public int OrderId { get; set; }
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("comment")]
        [MaxLength(1000)]
        public string Comment { get; set; }
        [Column("is_confirmed")]
        public bool IsConfirmed { get; set; } = false;
        public virtual Employee EmployeeNavigation { get; set; }
        public virtual Order OrderNavigation { get; set; }
    }

    public class OrderJobType
    {
        [Column("order_detail_id")]
        public int OrderDetailId { get; set; }
        [Column("type")]
        public string Type { get; set; }
        [Column("percent_contribute")]
        public double PercentContribute { get; set; }

        public virtual OrderDetail OrderDetailNavigation { get; set; }
    }

    public class OrderReceivedFee
    {
        [Column("order_id")]
        public int OrderId { get; set; }
        [Column("collecter_id")]
        public int CollecterId { get; set; }
        [Column("receiver_id")]
        public int ReceiverId { get; set; }
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("received_fee")]
        public double ReceivedFee { get; set; }
        [Column("fee_image")]
        public string FeeImg { get; set; }
        [Column("creater_id")]
        public int CreaterId { get; set; }
        public virtual Order OrderNavigation { get; set; }
        public virtual Employee CollecterNavigation { get; set; }
        public virtual Employee ReceiverNavigation { get; set; }
        public virtual Employee CreaterNavigation { get; set; }

        [NotMapped]
        public string File { get; set; }
    }

    public class OrderDetail
    {
        public OrderDetail()
        {
            OrderJobTypes = new List<OrderJobType>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("employee_id")]
        public int EmployeeId { get; set; }
        [Column("order_id")]
        public int OrderId { get; set; }
        [Column("is_confirmed")]
        public bool IsConfirmed { get; set; } = false;
        public virtual Employee EmployeeNavigation { get; set; }
        public virtual Order OrderNavigation { get; set; }
        public virtual List<OrderJobType> OrderJobTypes { get; set; }
    }
}
