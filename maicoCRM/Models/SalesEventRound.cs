﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace maicoCRM.Models
{
    public class SalesEventRound
    {
        public SalesEventRound()
        {
            
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string NameRound { get; set; }

        [Column("start_date")]
        public DateTime StartDate { get; set; }

        [Column("end_date")]
        public DateTime EndDate { get; set; }
    }
}
