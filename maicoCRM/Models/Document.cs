﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace maicoCRM.Models
{
    public class Document
    {
        public Document()
        {
            DocumentNotes = new HashSet<DocumentNote>();
        }

        [Column("id")]
        public int Id { get; set; }

        [MaxLength(100)]
        [Column("name")]
        public string Name { get; set; }

        [Column("type")]
        public string DocumentType { get; set; }
        public virtual ICollection<DocumentNote> DocumentNotes { get; set; }
        public virtual Project ProjectNav { get; set; }
    }

    public class DocumentNote
    {
        [Column("employee_id")]
        public int EmployeeId { get; set; }

        [Column("document_id")]
        public int DocumentId { get; set; }

        [Column("comment")]
        public string Comment { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("imgsrc", TypeName = "jsonb")]
        public ImageSource ImgSrc { get; set; }

        public virtual Employee EmployeeNavigation { get; set; }
        public virtual Document DocumentNavigation { get; set; }

    }
}