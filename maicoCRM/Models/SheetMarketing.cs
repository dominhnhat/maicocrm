﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace maicoCRM.Models
{
    public class SheetMarketing
    {
        public SheetMarketing()
        {
            Bookers = new HashSet<Employee>();
            BookerSheets = new HashSet<BookerSheet>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("creater_id")]
        public int CreaterId { get; set; }
        [Column("project_id")]
        public int ProjectId { get; set; }
        [Column("customer_id")]
        public int? CustomerId { get; set; }
        [Column("date_created")]
        public DateTime DateCreated { get; set; }
        [Column("info")]
        public string Info { get; set; }
        [Column("phonenumber")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        [StringLength(11, MinimumLength = 10, ErrorMessage = "Số điện thoại phải là 10-11 số")]
        public string PhoneNumber { get; set; }
        [Column("need")]
        public string Need { get; set; }
        [Column("source")]
        public string Source { get; set; }
        [NotMapped]
        public string CustomerName { get; set; }
        public virtual Employee CreaterNavigation { get; set; }
        public Project ProjectNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public ICollection<Employee> Bookers { get; set; }
        public ICollection<BookerSheet> BookerSheets { get; set; }
    }

    public class BookerSheet
    {
        [Column("booker_id")]
        public int BookerId { get; set; }
        [Column("sheetmarketing_id")]
        public int SheetMarketingId { get; set; }

        public Employee Booker { get; set; }
        public SheetMarketing SheetMarketing { get; set; }
    }
}
