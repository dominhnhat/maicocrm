﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace maicoCRM.Models
{
    public class SalesReport
    {
        [Column("month")]
        public DateTime Month { get; set; }

        [Column("sales_goals")]
        public double SalesGoals { get; set; }

        [Column("employee_id")]
        public int EmployeeId { get; set; }

        public Employee EmployeeNavigation { get; set; }
    }

    public class WeeklySalesReport
    {
        [Column("duration_id")]
        public int DurationId { get; set; }
        [Column("employee_id")]
        public int EmployeeId { get; set; }

        [Column("sales_goals")]
        public double SalesGoals { get; set; }

        public WeeklySalesReportDuration DurationNavigation { get; set; }
        public Employee EmployeeNavigation { get; set; }
    }
    public class WeeklySalesReportDuration
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("date_start")]
        public DateTime DateStart { get; set; }

        [Column("date_end")]
        public DateTime? DateEnd { get; set; }

        [Column("month")]
        public DateTime Month { get; set; }
        public List<WeeklySalesReport> Reports { get; set; }

        
    }
}
