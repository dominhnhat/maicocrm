﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace maicoCRM.Models
{
    public class Employee
    {
        public Employee()
        {
            CreatedCustomers = new HashSet<Customer>();
            ExploitedCustomerInformations = new HashSet<ExploitedCustomerInformation>();
            ExploitedLegalInformations = new HashSet<ExploitedLegalInformation>();
            SchedulesCreated = new HashSet<ScheduleCustomer>();
            Orders = new HashSet<Order>();
            OrderDetails = new HashSet<OrderDetail>();
            TakingCareCustomers = new HashSet<Customer>();
            SalesReportsNavigation = new HashSet<SalesReport>();
            WeeklySalesReports = new HashSet<WeeklySalesReport>();

            DocumentNotes = new HashSet<DocumentNote>();

            SheetMarketings = new HashSet<SheetMarketing>();
            BookerSheets = new HashSet<BookerSheet>();
            SheetBookeds = new HashSet<SheetMarketing>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        [MaxLength(100)]
        public string Name { get; set; }
        [Column("email")]
        [MaxLength(100)]
        public string Email { get; set; }
        [Column("office_id")]
        public int? OfficeId { get; set; }
        [Column("department_id")]
        public virtual int DepartmentId { get; set; }

        [Column("configration", TypeName = "jsonb")]
        public Configration Configration { get; set; } = new();

        [Column("is_blocked")]
        public bool IsBlocked { get; set; }

        [Column("blocked_customer_status")]
        public List<string> BlockedStatus { get; set; }

        public virtual Office OfficeNavigation { get; set; }
        public virtual Department DepartmentNavigation { get; set; }
        public virtual ICollection<Customer> CreatedCustomers { get; set; }
        public virtual ICollection<Customer> TakingCareCustomers { get; set; }
        public virtual ICollection<ExploitedCustomerInformation> ExploitedCustomerInformations { get; set; }
        public virtual ICollection<ScheduleCustomer> SchedulesCreated { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
        public virtual ICollection<ScheduleCustomer> SchedulesExecute { get; set; }
        public virtual ICollection<ExploitationLog> ExploitationLogs { get; set; }
        public virtual ICollection<SalesReport> SalesReportsNavigation { get; set; }
        public virtual ICollection<WeeklySalesReport> WeeklySalesReports { get; set; }
        public virtual ICollection<OrderComment> OrderComments { get; set; }
        public virtual List<OrderReceivedFee> CollecterOrderReceivedFees { get; set; }
        public virtual List<OrderReceivedFee> ReceiverOrderReceivedFees { get; set; }
        public virtual List<OrderReceivedFee> CreaterOrderReceivedFees { get; set; }
        public virtual ICollection<DocumentNote> DocumentNotes { get; set; }
        public virtual ICollection<SheetMarketing> SheetMarketings { get; set; }
        public virtual ICollection<SheetMarketing> SheetBookeds { get; set; }
        public virtual ICollection<BookerSheet> BookerSheets { get; set; }
        public virtual ICollection<ExploitedLegalInformation> ExploitedLegalInformations { get; set; }
        public virtual ICollection<ExploitationLegalLog> ExploitedLegalLogs { get; set; }
    }
    public class Configration
    {
        public double MinuteNotification { get; set; }
    }
}
