﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace maicoCRM.Models
{
    public class Area
    {
        public Area()
        {
            Projects = new HashSet<Project>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
