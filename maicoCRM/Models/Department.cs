﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace maicoCRM.Models
{
    public class Department
    {
        public Department()
        {
            EmployeesNav = new HashSet<Employee>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        public virtual ICollection<Employee> EmployeesNav { get; set; }


    }
}
