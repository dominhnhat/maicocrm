﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Models
{
    public class ScheduleCustomer
    {
        public ScheduleCustomer()
        {
            //Followers = new List<Follower>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("creator_id")]
        public int CreatorId { get; set; } //Id người tạo lịch hẹn   

        [Column("executor_id")]
        public int ExecutorId { get; set; }

        [Column("customer_id")]
        public int CustomerId { get; set; }

        [Column("time_start")]
        public DateTime TimeStart { get; set; }

        [Column("time_end")]
        public DateTime TimeEnd { get; set; }

        [Column("status")]  //trạng thái lịch hẹn
        [MaxLength(20)]
        public string Status { get; set; }

        [Column("comment")]
        [MaxLength(1000)]
        public string Comment { get; set; } //Nhận định khách hàng

        [Column("project_id")]
        public int ProjectId { get; set; }

        [Column("followers", TypeName = "jsonb")]
        public List<Follower> Followers { get; set; } //Id người được ủy quyền

        [Column("solution")]
        [MaxLength(2000)]
        public string Solution { get; set; }//Phương án giải quyết

        [Column("job_type")]
        [MaxLength(100)]
        public string JobType { get; set; } //Loại công việc

        public virtual Employee CreatorNavigation { get; set; }
        public virtual Employee ExecutorNavigation { get; set; }
        public virtual Customer CustomerNavigation { get; set; }
        public virtual Project ProjectNavigation { get; set; }
    } 
    public class Follower
    {
       public int Id { get; set; }
       public string Name { get; set; }
    }
}
