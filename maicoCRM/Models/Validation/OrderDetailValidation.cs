﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Models.Validation
{
    public class OrderDetailValidation:AbstractValidator<OrderDetail>
    {
        public OrderDetailValidation()
        { 
            RuleFor(x => x.EmployeeNavigation).NotEmpty().WithMessage("Bạn phải chọn");
        } 
    }
}
