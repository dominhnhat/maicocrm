﻿using FluentValidation; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Models.Validation
{
    public class OrderValidation: AbstractValidator<Order>
    {
        public OrderValidation()
        {
            RuleFor(x => x.BrokerageFee).NotEmpty().WithMessage("Bạn phải nhập tổng phí của đơn hàng!"); 
            RuleFor(x => x.ContractType).NotEmpty().WithMessage("Bạn phải lựa chọn loại hợp đồng!");
            RuleFor(x => x.ProjectNavigation).NotEmpty().WithMessage("Bạn phải lựa chọn loại dự án!");
            RuleForEach(x => x.OrderDetails).SetValidator(new OrderDetailValidation()); 
        }
    }
}
