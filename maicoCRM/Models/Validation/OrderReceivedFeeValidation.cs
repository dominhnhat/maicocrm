﻿using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Models.Validation
{
    public class OrderReceivedFeeValidation : AbstractValidator<OrderReceivedFee>
    {
        public OrderReceivedFeeValidation()
        {
            RuleFor(x => x.ReceivedFee).NotEmpty().WithMessage("Bạn chưa nhập phí đã nhận");
            RuleFor(x => x.ReceiverNavigation).NotEmpty().WithMessage("Bạn chưa chọn người nhận phí");
            RuleFor(x => x.CollecterNavigation).NotEmpty().WithMessage("Bạn chưa chọn người thu phí");
        }
    }
}
