﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Models.Validation
{
    public class SheetMarketingValidattion : AbstractValidator<SheetMarketing>
    {
        public SheetMarketingValidattion()
        {
            RuleFor(s => s.PhoneNumber).NotEmpty().WithMessage("Số điện thoại không được trống");
            RuleFor(s => s.PhoneNumber).Matches(@"^\d+$").WithMessage("Không được nhập chữ");
            RuleFor(s => s.PhoneNumber).Length(10, 11).WithMessage("Số điện thoại phải gồm 10 hoặc 11 số");
            RuleFor(s => s.Info).NotEmpty().WithMessage("Thông tin không được trống");
            RuleFor(s => s.CustomerName).NotEmpty().WithMessage("Tên khách hàng không được trống");
            RuleFor(s => s.ProjectNavigation).NotEmpty().WithMessage("Bạn phải chọn dự án");
            RuleFor(s => s.Need).NotEmpty().WithMessage("Bạn muốn mua hay thuê?");
            RuleFor(s => s.Source).NotEmpty().WithMessage("Khách hàng ở kênh nào?");
        }
    }
}
