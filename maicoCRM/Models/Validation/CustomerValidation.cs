﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FluentValidation;
using maicoCRM.Models;
using maicoCRM.Data;

namespace maicoCRM.Models.Validation
{
    public class CustomerValidation : AbstractValidator<Customer>
    {
        public CustomerValidation()
        {
            RuleFor(customer => customer.Name).NotEmpty().WithMessage("Không được phép để tên trống");
            RuleFor(customer => customer.Status).NotEmpty().WithMessage("Bạn phải chọn mối quan hệ");
            RuleFor(customer => customer.ProjectNavigation).NotEmpty().WithMessage("Bạn phải chọn dự án");
            RuleFor(customer => customer.Need).NotEmpty().WithMessage("Bạn muốn mua hay thuê?");
        }  
    }
}
