﻿using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Models.Validation
{
    public class ScheduleCustomerValidation:AbstractValidator<ScheduleCustomer>
    {
        public ScheduleCustomerValidation()
        {
            RuleFor(x => x.Solution).NotEmpty().WithMessage("Bạn chưa nhập phương án giải quyết");
            RuleFor(x => x.JobType).NotEmpty().WithMessage("Vui lòng chọn loại công việc");
            RuleFor(x => x.ProjectNavigation).NotEmpty().WithMessage("Vui lòng chọn dự án!");
            RuleFor(x => x.ExecutorNavigation).NotEmpty().WithMessage("Phải chọn người thực hiện");
        }
    }
}
