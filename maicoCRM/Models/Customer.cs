﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Models
{
    public class Customer
    {
        public Customer()
        {
            ExploitedCustomerInformations = new HashSet<ExploitedCustomerInformation>();
            ScheduleCustomers = new HashSet<ScheduleCustomer>();
            Orders = new HashSet<Order>();
            SheetMarketings = new HashSet<SheetMarketing>();
        }
        [Column("id")] 
        public int Id { get; set; }
        [Column("phone_number", TypeName = "jsonb")]
        public List<string> PhoneNumber { get; set; } = new();
        [Column("name")]
        [MaxLength(200)]
        public string Name { get; set; }
        [Column("curator_id")]
        public int CuratorId { get; set; }
        [Column("need")]
        [MaxLength(20)]
        public string Need { get; set; } //Mua hay Thue.
        [Column("description")]
        [MaxLength(1000)]
        public string Description { get; set; }//Mo Ta nhu cau cua khach.
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("creator_id")]
        public int CreatorId { get; set; }
        [Column("project_interested")]
        public int ProjectInterestedId { get; set; }
        public string Status { get; set; } // Moi quan he
        [Column("is_delete")]
        public bool? IsDelete { set; get; }// true = da xoa, false = dang duoc su dung
        public virtual Project ProjectNavigation { get; set; }
        public virtual Employee CreatorNavigation { get; set; }
        public virtual Employee CuratorNavigation { get; set; }
        public virtual ICollection<ExploitedCustomerInformation> ExploitedCustomerInformations { get; set; }
        public virtual ICollection<ScheduleCustomer> ScheduleCustomers { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<ExploitationLog> ExploitationLogs { get; set; }
        public virtual ICollection<SheetMarketing> SheetMarketings { get; set; }
    }
}
