﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace maicoCRM.Models
{
    public class LegalRecord
    {
        public LegalRecord()
        {
            ExploitedLegalInformations = new HashSet<ExploitedLegalInformation>();
        }
        [Column("id")]
        public int Id { get; set; }
        [Column("progress")]
        [MaxLength(20)]
        public string Progress { get; set; }
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("order_id")]
        public int OrderId { get; set; }
        public virtual Order OrderNavigation { get; set; }
        public virtual ICollection<ExploitedLegalInformation> ExploitedLegalInformations { get; set; }
        public virtual ICollection<ExploitationLegalLog> ExploitedLegalLogs { get; set; }
    }

    public class ExploitationLegalLog
    {
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("creator_id")]
        public int CreatorId { get; set; }
        [Column("legal_record_id")]
        public int LegalRecordId { get; set; }
        [Column("log")]
        [MaxLength(250)]
        public string Log { get; set; }
        public virtual Employee CreatorNavigation { get; set; }
        public virtual LegalRecord LegalRecordNavigation { get; set; }
    }

    public class ExploitedLegalInformation // Khai thác thông tin hồ sơ pháp lý của đơn hàng
    {
        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
        [Column("employee_id")]
        public int EmployeeId { get; set; }
        [Column("legal_record_id")]
        public int LegalRecordId { get; set; }
        [Column("comment")]
        public string Comment { get; set; }

        [Column("imgsrc", TypeName = "jsonb")]
        public ImageSource ImgSrc { get; set; }
        public virtual Employee EmployeeNavigation { get; set; }
        public virtual LegalRecord LegalRecordNavigation { get; set; }

        public class ImageSource
        {
            public List<string> Path { get; set; }
        }
    }
}
