﻿if (!window.Utilites) {
    window.Utilites = {};
}
window.Utilites = {
    CopyToClipboard: function(text) {
        var copyElement = document.createElement('input');
        copyElement.setAttribute('type', 'text');
        copyElement.setAttribute('value', text);
        copyElement = document.body.appendChild(copyElement);
        copyElement.select();
        document.execCommand('copy');
        copyElement.remove();
    },
    MakeHorizontalScroll: function(id) {
        //var item = document.getElementById(id);

        //item.addEventListener('wheel', function (e) {

        //    if (e.deltaY > 0) item.scrollLeft += 100;
        //    else item.scrollLeft -= 100;
        //});
        var outer = $('#' + id);
        $('#left-button').click(function () {
            var leftPos = outer.scrollLeft();
            outer.animate({
                scrollLeft: leftPos - 300
            }, 400);
        });

        $('#right-button').click(function () {
            var leftPos = outer.scrollLeft();
            outer.animate({
                scrollLeft: leftPos + 300
            }, 400);
        });
    },
    ScrollToTop: function () {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    },
    getHtmlContent: function (id) {
        var ele = document.getElementById(id);
        var content = ele.textContent || ele.innerText || "";
        this.CopyToClipboard(content);
    }
}

function ScrollModalToBottom(countItem, currentItem, height) {
    var heightDialog = document.getElementsByClassName("mud-dialog-content")[0].offsetHeight;
    var h = heightDialog + height;
    document.getElementsByClassName("mud-dialog-content")[0].style.height = h + "px";
    var $win = $(window);
    var scrollTarget = $win.height() + $win.scrollTop() + 1 >= $(document).height() ? 0 : $win.scrollTop() + $win.height();
    console.log(scrollTarget)
    if (screen.width > 500) {
        if (true) {
            $(".mud-dialog-content").animate({
                scrollTop: scrollTarget
            }, 600);
        }
    }
}

// Scroll dialog to bottom
function ScrollModalToBottom(countItem, currentItem, height) {
    var heightDialog = document.getElementsByClassName("mud-dialog-content")[0].offsetHeight;
    var h = heightDialog + height;
    document.getElementsByClassName("mud-dialog-content")[0].style.height = h + "px";
    var $win = $(window);
    var scrollTarget = $win.height() + $win.scrollTop() + 1 >= $(document).height() ? 0 : $win.scrollTop() + $win.height();
    if (screen.width > 500) {
        if (countItem <= 6 || currentItem >= 4) {
            $(".mud-dialog-content").animate({
                scrollTop: scrollTarget
            }, 600);
        }
    }
}

// Remove Property CSS
// property and className is List<string>
function removePropertyByClassName(property, className) {
    setTimeout(function () {
        for (let i = 0; i < property.length; i++) {
            document.getElementsByClassName(className[i])[0].style.removeProperty(property[i]);
        }
    }, 300);
}

// Scroll For DealInformation
function RightToleftScrollDealInfor() {
    var leftPos = $("#scroll-container").scrollLeft();
    var widthPaper = document.getElementById("paper-dealinfo").offsetWidth;
    $("#scroll-container").animate({
        scrollLeft: leftPos + widthPaper
    }, 200);
}

function LeftToRightScrollDealInfor() {
    var leftPos = $("#scroll-container").scrollLeft();
    var widthPaper = document.getElementById("paper-dealinfo").offsetWidth;
    $("#scroll-container").animate({
        scrollLeft: leftPos - widthPaper
    }, 200);
}

function DisableScrollDefault(id) {
    $("#" + id).bind("touchmove", function (e) {
        e.preventDefault();
    });
}

function ScrollToLastItemDealInfor(countOrderDetail) {
    var leftPos = $("#paper-dealinfo").scrollLeft();
    var widthPaper = document.getElementById("paper-dealinfo").offsetWidth;
    $("#scroll-container").animate({
        scrollLeft: leftPos + (countOrderDetail - 1) * widthPaper
    }, 400);
}

//troll
window.minhdb = true;
function MoveButtons (classBtnYes, classBtnNo) {
    var btnYes = $('.' + classBtnYes)
    var btnNo = $('.' + classBtnNo)

    btnYes.hover(function () {
        if (minhdb) {
            $(this).animate({ left: '380px' }, 150)
            btnNo.animate({ left: '-380px' }, 150)
        }
        else {
            $(this).animate({ left: '0px' }, 150);
            btnNo.animate({ left: '0px' }, 150);
        }
    }, function () {
            minhdb = !minhdb;
    })
}