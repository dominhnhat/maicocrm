﻿if (!window.ChartFunctions) {
    window.ChartFunctions = {};
}

window.BlazorCharts = new Map();

window.ChartFuntions = {
    createPie: function (id, label, values) {
        var total = 0;
        var percent = [];
        values.forEach(item => total += item);
        values.forEach(item => percent.push(item / total * 100));

        var ctx = document.getElementById(id).getContext('2d');
        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: percent,
                    backgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)'
                    ]
                }],
                labels: label
            },
            options: {
                responsive: true,
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var label = values[tooltipItem.index] || '';
                            return label;
                        }
                    }
                },
                legend: {
                    position: 'right',
                }
            }
        };

        // Initial Chart
        var pieChart = new Chart(ctx, config);
    },

    createMixedChart: function (id, label, barData, lineData, labels) {
        var names = this.getShortName(labels);
        var dataset1 = this.calculatePercent(barData);
        var dataset2 = this.calculatePercent(lineData);

        var ctx = document.getElementById(id).getContext('2d');
        var config = {
            type: 'bar',
            data: {
                labels: names,
                datasets: [{
                    data: dataset1,
                    label: label,
                    backgroundColor: 'rgba(255, 99, 132, 0.4)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                }, {
                    type: 'line',
                    data: dataset2,
                    label: 'Doanh số',
                    backgroundColor: 'rgba(54, 162, 235, 0.4)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                //duy tri kich thuoc ban dau: true, khi chinh ve false thi no se theo height cua div cha ---Chi Linh
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            precision: 0
                        }
                    }]
                },
                legend: {
                    position: 'bottom',
                    display: true
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].label;
                            if (label && tooltipItem.datasetIndex === 0)
                                return label + ': ' + barData[tooltipItem.index];
                            else if (label && tooltipItem.datasetIndex === 1)
                                return label + ': ' + lineData[tooltipItem.index].toLocaleString();
                        }
                    }
                },
                animation: {
                    onComplete: function () {
                        var chartInstance = this.chart;
                        var ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset, i) {
                            if (i === 0) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = barData[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            }
                        });
                    }
                }
            }
        };

        // Initial Chart
        var mixedChart = new Chart(ctx, config);
        window.BlazorCharts.set(id, mixedChart);
    },

    updateMixedChart: function (id, barData, lineData, labels) {
        var chart = window.BlazorCharts.get(id);
        var newLabels = this.getShortName(labels);
        var dataset1 = this.calculatePercent(barData);
        var dataset2 = this.calculatePercent(lineData);

        chart.data.labels = newLabels;
        chart.data.datasets[0].data = dataset1;
        chart.data.datasets[1].data = dataset2;

        chart.options.tooltips.callbacks.label = function (tooltipItem, data) {
            var label = data.datasets[tooltipItem.datasetIndex].label;
            if (tooltipItem.datasetIndex === 0)
                return label + ': ' + barData[tooltipItem.index];
            else if (tooltipItem.datasetIndex === 1)
                return label + ': ' + lineData[tooltipItem.index].toLocaleString();
        }

        chart.options.animation.onComplete = function () {
            var chartInstance = this.chart;
            var ctx = chartInstance.ctx;
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            this.data.datasets.forEach(function (dataset, i) {
                if (i === 0) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function (bar, index) {
                        var data = barData[index];
                        ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                }
            });
        }

        chart.update();
    },
    createMarketingColumnChart: function (id, buys, hires, others, labels) {
        var ctx = document.getElementById(id);

        var config = {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    data: buys,
                    label: 'Mua',
                    backgroundColor: 'rgba(255, 99, 132, 0.4)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1

                }, {
                    data: hires,
                    label: 'Thuê',
                    backgroundColor: 'rgba(54, 162, 235, 0.4)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }, {
                    data: others,
                    label: 'KGV',
                    backgroundColor: 'rgba(248,187,2,0.4)',
                    borderColor: 'rgba(248,187,2,1)',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            precision: 0
                        }
                    }]
                },
                plugins: {
                    title: {
                        display: true,
                        text: ''
                    },
                },

            }
        };

        // Initial Chart
        var columnChart = new Chart(ctx, config);
        window.BlazorCharts.set(id, columnChart);
    },
    updateMarketingColumnChart: function (id, buys, hires, others, labels) {
        var chart = window.BlazorCharts.get(id);

        chart.data.labels = labels;
        chart.data.datasets[0].data = buys;
        chart.data.datasets[1].data = hires;
        chart.data.datasets[2].data = others;

        chart.update();
    },

    createScheduleCustomerChart: function (id, bubbleData) {
        var ctx = document.getElementById(id);
        var label = "";
        var backgroundColor = "";
        var boderColor = "";

        if (id == "success") {
            label = "Thành công"
            backgroundColor = 'rgba(30, 213, 72, 0.4)';
            boderColor = 'rgba(30, 213, 72, 1)';
        } else {
            label = "Bị hủy"
            backgroundColor = 'rgba(255,26,104,0.4)';
            boderColor = 'rgba(255,26,104,1)';
        }

        var config = {
            type: 'bubble',
            data: {
                datasets: [{
                    data: bubbleData,
                    label: label,
                    backgroundColor: backgroundColor,
                    borderColor: boderColor,
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            precision: 0,
                            userCallback: function (label, index, labels) {
                                
                                return label + 'h';
                            },
                            fontColor: 'blue',
                            padding:20
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            precision: 0,
                            userCallback: function (label, index, labels) {
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                return days[label];
                            },
                            fontColor: 'red',
                        },
                    }],
                }
            },
            
            plugins: {
                title: {
                    display: true,
                    text: ''
                }
            },

        };


        // Initial Chart
        var columnChart = new Chart(ctx, config);
        window.BlazorCharts.set(id, columnChart);
    },
    updateScheduleCustomerChart: function (id, data) {
        var chart = window.BlazorCharts.get(id);

        chart.data.datasets[0].data = data;
        chart.update();
    },
    getShortName: function (names) {
        var shortNames = [];
        names.forEach(item => {
            let works = item.trim().split(" ");
            let name = works[works.length - 2] + " " + works[works.length - 1];
            shortNames.push(name);
        });
        return shortNames;
    },

    calculatePercent: function (data) {
        var dataset = [];
        var max = Math.max(...data);
        max += max * 10 / 100;
        data.forEach(item => dataset.push(Math.round((item * 100 / max) * 100) / 100));
        return dataset;
    }
};