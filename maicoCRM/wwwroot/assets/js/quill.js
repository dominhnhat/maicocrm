﻿if (!window.QuillFunctions) {
    window.QuillFunctions = {};
}
window.QuillFunctions = {
    createQuill: function (quillElement,idToolbar) {
        var options = {
            debug: 'info',
            modules: {
                toolbar: '#'+idToolbar
            },
            placeholder: 'Mời bạn nhập...',
            readOnly: false,
                
            theme: 'snow'
        };
        // set quill at the object we can call
        // methods on later
        new Quill(quillElement, options);
    }, 
    getQuillHTML: function (quillControl) { 
        return quillControl.__quill.root.innerHTML;
    },
    deleteQuillContent: function (quillControl) {
        var emptyContent = '[{ "insert": "\n" }]'; 
        quillControl.__quill.setContents(emptyContent, 'api');
    },
    setHTMLContents: function (quillControl, content) {
        var delta = quillControl.__quill.clipboard.convert(content);
        quillControl.__quill.setContents(delta, 'silent');
    }
};

