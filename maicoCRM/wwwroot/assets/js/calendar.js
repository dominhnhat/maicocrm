﻿function Calendar(e, viewScreen) { 
    var temp = document.getElementById("calendar");
    var valueView = '';
    var headerLeft = '';
    var headerRight = '';
    if (viewScreen == 'list') {
        valueView = 'listWeek';
        headerLeft = '';
        headerRight = 'prev,next';
    }
    else {
        valueView = 'dayGridMonth';
        headerLeft = 'dayGridMonth,timeGridWeek,timeGridDay today';
        headerRight = 'prevYear,prev,next,nextYear';
    }
    var calendar = new FullCalendar.Calendar(temp, { 
        initialView: valueView,
        locale: 'vi',
        firstDay: 1,
        height: 600,
        headerToolbar: {
            left: headerLeft,
            center: 'title',
            right: headerRight
        }, 
        weekNumbers: true, 
        dayMaxEvents: true, 
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: e
    });
    calendar.render(); 
};