﻿using maicoCRM.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Data
{
    public class MaicoCRMContext : DbContext
    {
        public MaicoCRMContext(DbContextOptions<MaicoCRMContext> options) : base(options)
        { }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ExploitedCustomerInformation> ExploitedCustomerInformations { get; set; }
        public DbSet<ScheduleCustomer> ScheduleCustomers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<ExploitationLog> ExploitationLogs { get; set; }
        public DbSet<SalesReport> SalesReports { get; set; }
        public DbSet<WeeklySalesReport> WeeklySalesReports { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<WeeklySalesReportDuration> WeeklySalesReportDurations { get; set; }
        public DbSet<OrderComment> OrderComments { get; set; }
        public DbSet<OrderReceivedFee> OrderReceivedFees { get; set; }
        public DbSet<OrderJobType> OrderJobTypes { get; set; }
        public DbSet<SheetMarketing> SheetMarketings { get; set; }
        public DbSet<SalesEventRound> SalesEventRounds { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentNote> DocumentNotes { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<LegalRecord> LegalRecords { get; set; }
        public DbSet<ExploitedLegalInformation> ExploitedLegalInformations { get; set; }
        public DbSet<ExploitationLegalLog> ExploitationLegalLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("pk_Employee");
            }
            );
            modelBuilder.Entity<Document>(entity => {
                entity.HasKey(c => c.Id)
                    .HasName("pk_Document");
            });
            modelBuilder.Entity<Area>(entity =>
            {
                entity.HasKey(c => c.Id)
                        .HasName("pk_Area");
            });
            modelBuilder.Entity<Project>(entity =>
                {
                    entity.HasKey(c => c.Id)
                            .HasName("pk_Project");

                    entity.HasOne<Area>(s => s.AreaNavigation)
                        .WithMany(c => c.Projects)
                        .HasForeignKey(e => e.AreaId)
                        .OnDelete(DeleteBehavior.SetNull)
                        .HasConstraintName("fk_Project_Area");
                        
                    entity.HasOne(c => c.DocumentNavigation)
                        .WithOne(c => c.ProjectNav)
                        .HasForeignKey<Project>(c => c.DocumentId)
                        .OnDelete(DeleteBehavior.Cascade)
                        .HasConstraintName("fk_project_document");
                }
            );
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasQueryFilter(x => x.IsDelete != true);
                entity.Property(x => x.IsDelete).HasDefaultValue(false);
                entity.HasKey(e => e.Id)
                      .HasName("pk_Customer");
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(c => c.CreatorNavigation)
                    .WithMany(c => c.CreatedCustomers)
                    .HasForeignKey(e => e.CreatorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Customer_Employee");

                entity.HasOne(c => c.CuratorNavigation)
                    .WithMany(c => c.TakingCareCustomers)
                    .HasForeignKey(c => c.CuratorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Customer_Curator");

                entity.HasOne(e => e.ProjectNavigation)
                    .WithMany(e => e.Customers)
                    .HasForeignKey(e => e.ProjectInterestedId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Customer_Project");
            });
            modelBuilder.Entity<ExploitedCustomerInformation>(entity =>
            {
                entity.HasKey(e => new { e.CreatedAt, e.CustomerId, e.EmployeeId });
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");
                entity.HasOne(c => c.CustomerNavigation)
                    .WithMany(c => c.ExploitedCustomerInformations)
                    .HasForeignKey(e => e.CustomerId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitedCustomerInformation_Customer");

                entity.HasOne(e => e.EmployeeNavigation)
                    .WithMany(e => e.ExploitedCustomerInformations)
                    .HasForeignKey(e => e.EmployeeId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitedCustomerInformation_Employee");
            });
            modelBuilder.Entity<ExploitedLegalInformation>(entity =>
            {
                entity.HasKey(e => new { e.CreatedAt, e.LegalRecordId, e.EmployeeId });
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");
                entity.HasOne(c => c.LegalRecordNavigation)
                    .WithMany(c => c.ExploitedLegalInformations)
                    .HasForeignKey(e => e.LegalRecordId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitedLegalInformation_LegalRecord");

                entity.HasOne(e => e.EmployeeNavigation)
                    .WithMany(e => e.ExploitedLegalInformations)
                    .HasForeignKey(e => e.EmployeeId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitedLegalInformation_Employee");
            });
            modelBuilder.Entity<ScheduleCustomer>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.HasOne(e => e.CustomerNavigation)
                    .WithMany(e => e.ScheduleCustomers)
                    .HasForeignKey(e => e.CustomerId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ScheduleCustomer_Customer");

                entity.HasOne(e => e.ProjectNavigation)
                    .WithMany(e => e.ScheduleCustomers)
                    .HasForeignKey(e => e.ProjectId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ScheduleCustomer_Project");

                entity.HasOne(e => e.CreatorNavigation)
                    .WithMany(e => e.SchedulesCreated)
                    .HasForeignKey(e => e.CreatorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ScheduleCustomer_Creator");

                entity.HasOne(e => e.ExecutorNavigation)
                    .WithMany(e => e.SchedulesExecute)
                    .HasForeignKey(e => e.ExecutorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_scheduleCustomer_Executor");

            });
            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");
                entity.Property(c => c.IsConfirmed)
                    .HasDefaultValue(false);
                entity.Property(c => c.IsActive)
                    .HasDefaultValue(true);

                entity.HasOne(e => e.CustomerNavigation)
                    .WithMany(e => e.Orders)
                    .HasForeignKey(e => e.CustomerId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Order_Customer");
                entity.HasOne(e => e.EmployeeNavigation)
                    .WithMany(e => e.Orders)
                    .HasForeignKey(e => e.CreatorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Order_Employee");
                entity.HasOne(e => e.ProjectNavigation)
                    .WithMany(e => e.Orders)
                    .HasForeignKey(e => e.ProjectId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Order_Project");
            });
            modelBuilder.Entity<LegalRecord>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(e => e.OrderNavigation)
                    .WithOne(e => e.LegalRecord)
                    .HasForeignKey<LegalRecord>(e => e.OrderId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_LegalRecord_Order");
            });
            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.HasKey(e => new { e.Id });
                entity.HasIndex(e => new { e.Id, e.OrderId, e.EmployeeId }).IsUnique();
                entity.Property(c => c.IsConfirmed)
                    .HasDefaultValue(false);
                entity.HasOne(e => e.EmployeeNavigation)
                .WithMany(e => e.OrderDetails)
                    .HasForeignKey(e => e.EmployeeId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_OrderDetail_Employee");
                entity.HasOne(e => e.OrderNavigation)
                .WithMany(e => e.OrderDetails)
                    .HasForeignKey(e => e.OrderId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_OrderDetail_Order");
            });
            modelBuilder.Entity<ExploitationLog>(entity =>
            {
                entity.HasKey(c => new { c.CreatedAt, c.CreatorId, c.CustomerId });
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(c => c.CustomerNavigation)
                    .WithMany(c => c.ExploitationLogs)
                    .HasForeignKey(c => c.CustomerId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitationLogs_Customer");

                entity.HasOne(c => c.CreatorNavigation)
                    .WithMany(c => c.ExploitationLogs)
                    .HasForeignKey(c => c.CreatorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitationLogs_Employee");
            });
            modelBuilder.Entity<ExploitationLegalLog>(entity =>
            {
                entity.HasKey(c => new { c.CreatedAt, c.CreatorId, c.LegalRecordId });
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(c => c.CreatorNavigation)
                    .WithMany(c => c.ExploitedLegalLogs)
                    .HasForeignKey(c => c.CreatorId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitedLegalLogs_Employee");

                entity.HasOne(c => c.LegalRecordNavigation)
                    .WithMany(c => c.ExploitedLegalLogs)
                    .HasForeignKey(c => c.LegalRecordId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_ExploitedLegalLogs_LegalRecord");
            });
            modelBuilder.Entity<SalesReport>(entity =>
            {
                entity.HasKey(c => new { c.EmployeeId, c.Month });
                entity.HasOne(c => c.EmployeeNavigation)
                    .WithMany(c => c.SalesReportsNavigation)
                    .HasForeignKey(c => c.EmployeeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_SalesReport_Employee");
            });
            modelBuilder.Entity<WeeklySalesReport>(entity =>
            {
                entity.HasKey(c => new { c.EmployeeId, c.DurationId });

                entity.HasOne(c => c.DurationNavigation)
                    .WithMany(c => c.Reports)
                    .HasForeignKey(c => c.DurationId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_weeklysalesreport_duration");

                entity.HasOne(c => c.EmployeeNavigation)
                    .WithMany(c => c.WeeklySalesReports)
                    .HasForeignKey(c => c.EmployeeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_weeklysalesreports_employee");
            });
            modelBuilder.Entity<Office>(entity =>
            {
                entity.HasKey(c => c.Id);
                entity.HasMany(c => c.EmployeesNav)
                    .WithOne(c => c.OfficeNavigation)
                    .HasForeignKey(c => c.OfficeId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_employee_office");
            });
            modelBuilder.Entity<Department>(entity =>
            {
                entity.HasKey(c => c.Id);
                entity.HasMany(c => c.EmployeesNav)
                    .WithOne(c => c.DepartmentNavigation)
                    .HasForeignKey(c => c.DepartmentId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_employee_department");
            });
            modelBuilder.Entity<OrderComment>(entity =>
            {
                entity.HasKey(e => new { e.OrderId, e.EmployeeId, e.CreatedAt });
                entity.Property(c => c.IsConfirmed)
                    .HasDefaultValue(false);
                entity.HasOne(e => e.EmployeeNavigation)
                .WithMany(e => e.OrderComments)
                    .HasForeignKey(e => e.EmployeeId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_OrderComment_Employee");
                entity.HasOne(e => e.OrderNavigation)
                .WithMany(e => e.OrderComments)
                    .HasForeignKey(e => e.OrderId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_OrderComment_Order");
            });
            modelBuilder.Entity<OrderReceivedFee>(entity =>
            {
                entity.HasKey(e => new { e.OrderId, e.CreatedAt });
                entity.Property(p => p.CreatedAt)
                     .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(e => e.OrderNavigation)
                    .WithMany(e => e.OrderReceivedFees)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_OrderReceivedFee_Order");
                entity.HasOne(e => e.ReceiverNavigation)
                    .WithMany(e => e.ReceiverOrderReceivedFees)
                    .HasForeignKey(e => e.ReceiverId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Receiver_OrderReceivedFee");
                entity.HasOne(e => e.CollecterNavigation)
                    .WithMany(e => e.CollecterOrderReceivedFees)
                    .HasForeignKey(e => e.CollecterId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("fk_Collecter_OrderReceivedFee");
                entity.HasOne(e => e.CreaterNavigation)
                   .WithMany(e => e.CreaterOrderReceivedFees)
                   .HasForeignKey(e => e.CreaterId)
                   .OnDelete(DeleteBehavior.SetNull)
                   .HasConstraintName("fk_Creater_OrderReceivedFee");
            });
            modelBuilder.Entity<OrderJobType>(entity =>
            {
                entity.HasKey(e => new { e.OrderDetailId, e.Type });

                entity.HasOne(e => e.OrderDetailNavigation)
                    .WithMany(e => e.OrderJobTypes)
                    .HasForeignKey(e => e.OrderDetailId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_OrderDetail_OrderJobType");
            });
            modelBuilder.Entity<DocumentNote>(entity =>
            {
                entity.HasKey(e => new { e.CreatedAt, e.DocumentId, e.EmployeeId });
                entity.Property(p => p.CreatedAt)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.HasOne(c => c.EmployeeNavigation)
                    .WithMany(c => c.DocumentNotes)
                    .HasForeignKey(c => c.EmployeeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_documentnote_employee");

                entity.HasOne(c => c.DocumentNavigation)
                    .WithMany(c => c.DocumentNotes)
                    .HasForeignKey(c => c.DocumentId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_documentnote_document");
            });

            modelBuilder.Entity<SheetMarketing>(entity =>
            {
                entity.HasMany<Employee>(e => e.Bookers)
                .WithMany(s => s.SheetBookeds)
                .UsingEntity<BookerSheet>(
                    j => j.HasOne(b => b.Booker)
                            .WithMany(s => s.BookerSheets)
                            .HasForeignKey(s => s.BookerId),
                    j => j.HasOne(b => b.SheetMarketing)
                            .WithMany(b => b.BookerSheets)
                            .HasForeignKey(b => b.SheetMarketingId)
                );


                entity.HasOne<Employee>(x => x.CreaterNavigation)
                .WithMany(x => x.SheetMarketings)
                .HasForeignKey(x => x.CreaterId);

                entity.HasOne<Customer>(c => c.CustomerNavigation)
                .WithMany(s => s.SheetMarketings)
                .HasForeignKey(c => c.CustomerId);

                entity.Property(p => p.DateCreated)
                      .HasDefaultValueSql("CURRENT_TIMESTAMP");

            });

        }

    }
}