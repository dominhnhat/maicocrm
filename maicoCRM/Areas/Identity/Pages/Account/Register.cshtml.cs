﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using maicoCRM.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using maicoCRM.Services;
using maicoCRM.Models;

namespace maicoCRM.Areas.Identity.Pages.Account
{
    [Authorize(Roles = "Admin")]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<maicoCRMUser> _signInManager;
        private readonly UserManager<maicoCRMUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly EmployeeServices _employeeServices;
        private readonly OfficeServices _officeServices;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly DepartmentServices _departmentServices;
        public RegisterModel(
            UserManager<maicoCRMUser> userManager,
            SignInManager<maicoCRMUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            EmployeeServices employeeServices,
            OfficeServices officeServices,
            RoleManager<IdentityRole> roleManager,
            DepartmentServices departmentServices)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _employeeServices = employeeServices;
            _officeServices = officeServices;
            _roleManager = roleManager;
            _departmentServices = departmentServices;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public List<Office> Offices { get; set; }
        public List<Department> Departments { get; set; }
        public List<IdentityRole> Roles { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [Display(Name = "Phòng ban")]
            public int Department { get; set; }

            [Required]
            [Display(Name = "Tên nhân viên")]
            public string UserName { get; set; }

            [Required]
            [Display(Name = "Văn phòng")]
            public int Office { get; set; }

            [Required]
            [Display(Name = "Quyền")]
            public string Role { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Mật khẩu")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Xác nhận mật khẩu")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
            
        }
        
        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            Offices = await _officeServices.GetAllAsync();
            Departments = await _departmentServices.GetAllAsync();
            Roles = _roleManager.Roles.ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/admin/danhsach-taikhoan");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new maicoCRMUser { UserName = Input.UserName, Email = Input.Email, EmailConfirmed = true, IsEnabled = true };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user,Input.Role);
                    //Create Employee
                    await CreateEmployee();

                    _logger.LogInformation("User created a new account with password.");
                    // var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    // code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    // var callbackUrl = Url.Page(
                    //     "/Account/ConfirmEmail",
                    //     pageHandler: null,
                    //     values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                    //     protocol: Request.Scheme);

                    // await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                    //     $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    // if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    // {
                    //     return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    // }
                    // else
                    // {
                        //await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    //}
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
        private async Task CreateEmployee(){
            Employee emp = new Employee {
                Email = Input.Email,
                Name = Input.UserName,
                OfficeId = Input.Office,
                DepartmentId = Input.Department
            };
            await _employeeServices.AddAsync(emp);
        }
    }
}
