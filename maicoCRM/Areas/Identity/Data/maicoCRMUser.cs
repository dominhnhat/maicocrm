﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace maicoCRM.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the maicoCRMUser class
    public class maicoCRMUser : IdentityUser
    {
        public bool IsEnabled { get; set; }
    }
}
