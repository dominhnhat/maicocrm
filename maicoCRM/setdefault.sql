﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);

START TRANSACTION;

ALTER TABLE "SheetMarketings" DROP COLUMN customer_name;

ALTER TABLE "Employees" ADD configration jsonb NULL;

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20210704141339_InitDb', '5.0.2');

COMMIT;

START TRANSACTION;

ALTER TABLE "SheetMarketings" ALTER COLUMN date_created SET DEFAULT (CURRENT_TIMESTAMP);

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20210704144554_InitDb2', '5.0.2');

COMMIT;

