﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.ViewModels
{
    public class SalesReportViewModel
    {
        public int EmployeeId { get; set; }
        public DateTime Month { get; set; }
        public string Name { get; set; }
        public double SalesGoals { get; set; }
        public double Sales { get; set; }
        public double Percent { get; set; }
        public string Salary { get; set; }
        public string Office { get; set; }
        public bool IsDisplayable { get; set; }
        public WeeklyReport WeeklyReports { get; set; } = new();

        public class WeeklyReport
        {
            public int ReportId { get; set; }
            public int Week { get; set; }
            public double SalesGoals { get; set; }
            public double Sales { get; set; }
            public double Percent { get; set; }
        }
    }
}
