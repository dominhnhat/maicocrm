﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.ViewModels
{
    public class WorkDashboardDataViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Office { get; set; }
        public int Commute { get; set; }
        public int SuccessSchedule { get; set; }
        public int Work { get; set; }
        public double Sale { get; set; }
    }
}
