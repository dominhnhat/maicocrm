﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace maicoCRM.Component.SalesEvent.Models
{
    public class EventSalesModel
    {
        public DateTime DateOfRound { get; set; }
        public List<ProjectSales> ListProjectSales { get; set; }
    }

    public class ProjectSales
    {
        public int OfficeId { get; set; }
        public List<string> ProjectNames { get; set; }
        public List<double> ListSales { get; set; }
    }
}
