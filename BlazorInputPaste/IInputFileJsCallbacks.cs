// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.JSInterop;

namespace BlazorInputPaste
{
    internal interface IInputFileJsCallbacks
    {
        IJSUnmarshalledRuntime JsUnmarshalledRuntime { get; set; }

        Task NotifyChange(BrowserFilePaste[] files);
    }
}
