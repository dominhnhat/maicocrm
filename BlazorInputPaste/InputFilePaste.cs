// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Rendering;
using Microsoft.Extensions.Options;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components;

namespace BlazorInputPaste
{
    /// <summary>
    /// A component that wraps the HTML file input element and supplies a <see cref="Stream"/> for each file's contents.
    /// </summary>
    public class InputFilePaste : ComponentBase, IInputFileJsCallbacks, IDisposable
    {
        protected ElementReference _inputFileElement;

        private IJSUnmarshalledRuntime? _jsUnmarshalledRuntime;

        private InputFileJsCallbacksRelay? _jsCallbacksRelay;

        [Inject]
        private IJSRuntime JSRuntime { get; set; } = default!;

        [Inject]
        private IOptions<RemoteBrowserFileStreamOptions> Options { get; set; } = default!;

        /// <summary>
        /// Gets or sets the event callback that will be invoked when the collection of selected files changes.
        /// </summary>
        [Parameter]
        public EventCallback<InputFileChangeEventArgs> OnChange { get; set; }

        /// <summary>
        /// Gets or sets a collection of additional attributes that will be applied to the input element.
        /// </summary>
        [Parameter(CaptureUnmatchedValues = true)]
        public IDictionary<string, object>? AdditionalAttributes { get; set; }

        [Parameter]
        public string PasteBlockId { get; set; }
        public IJSUnmarshalledRuntime JsUnmarshalledRuntime { get => _jsUnmarshalledRuntime; set => _jsUnmarshalledRuntime = value; }
        public IJSUnmarshalledRuntime JsUnmarshalledRuntime1 { get => _jsUnmarshalledRuntime; set => _jsUnmarshalledRuntime = value; }
        internal InputFileJsCallbacksRelay JsCallbacksRelay { get => _jsCallbacksRelay; set => _jsCallbacksRelay = value; }

        /// <inheritdoc/>
        protected override void OnInitialized()
        {
            JsUnmarshalledRuntime = JSRuntime as IJSUnmarshalledRuntime;
        }

        /// <inheritdoc/>
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                JsCallbacksRelay = new InputFileJsCallbacksRelay(this);
                await JSRuntime.InvokeVoidAsync(InputFileInterop.Init, JsCallbacksRelay.DotNetReference, _inputFileElement, PasteBlockId);
            }
        }

        /// <inheritdoc/>
        //protected override void BuildRenderTree(RenderTreeBuilder builder)
        //{
        //    builder.OpenElement(0, "input");
        //    builder.AddMultipleAttributes(1, AdditionalAttributes);
        //    builder.AddAttribute(2, "type", "file");
        //    builder.AddElementReferenceCapture(3, elementReference => _inputFileElement = elementReference);
        //    builder.CloseElement();
        //}

        internal Stream OpenReadStream(BrowserFilePaste file, CancellationToken cancellationToken)
            => JsUnmarshalledRuntime != null ?
                (Stream)new SharedBrowserFileStream(JSRuntime, JsUnmarshalledRuntime, _inputFileElement, file) :
                new RemoteBrowserFileStream(JSRuntime, _inputFileElement, file, Options.Value, cancellationToken);

        internal async ValueTask<IBrowserFile> ConvertToImageFileAsync(BrowserFilePaste file)
        {
            var imageFile = await JSRuntime.InvokeAsync<BrowserFilePaste>(InputFileInterop.ToImageFile, _inputFileElement, file.Id);

            if (imageFile is null)
            {
                throw new InvalidOperationException("ToImageFile returned an unexpected null result.");
            }

            imageFile.Owner = this;

            return imageFile;
        }

        Task IInputFileJsCallbacks.NotifyChange(BrowserFilePaste[] files)
        {
            foreach (var file in files)
            {
                file.Owner = this;
            }

            return OnChange.InvokeAsync(new InputFileChangeEventArgs(files));
        }

        void IDisposable.Dispose()
        {
            JsCallbacksRelay?.Dispose();
        }
    }
}
