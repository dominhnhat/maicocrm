// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components.Forms;

namespace BlazorInputPaste
{
    internal class InputFileJsCallbacksRelay : IDisposable
    {
        private readonly IInputFileJsCallbacks _callbacks;

        public IDisposable DotNetReference { get; }

        public InputFileJsCallbacksRelay(IInputFileJsCallbacks callbacks)
        {
            _callbacks = callbacks;

            DotNetReference = DotNetObjectReference.Create(this);
        }

        [JSInvokable]
        public Task NotifyChange(BrowserFilePaste[] files)
            => _callbacks.NotifyChange(files);

        public void Dispose()
        {
            DotNetReference.Dispose();
        }
    }
}
