﻿if (!window.InputPaste) {
    window.InputPaste = {};
}

window.InputPaste = {
    
    init: function init(callbackWrapper, elem, pasteBlockId) {
        elem._blazorFilesById = {};
        elem._blazorInputFileNextFileId = 0;
        elem.addEventListener('click', function () {
            // Permits replacing an existing file with a new one of the same file name.
            elem.value = '';
        });
        document.getElementById(pasteBlockId.toString()).onpaste = function (event) {
            // use event.originalEvent.clipboard for newer chrome versions
            var items = (event.clipboardData || event.originalEvent.clipboardData).items;
            // find pasted image among pasted items
            var blob = null;
            for (var i = 0; i < items.length; i++) {
                if (items[i].type.indexOf("image") === 0) {
                    blob = items[i].getAsFile();
                    event.preventDefault();
                }
            }
            var splitBlobName = blob.name.split('.');
            splitBlobName[0] = new Date().getTime();
            splitBlobName[1] = 'jpg';
            var fileName = splitBlobName.join('.').toString();
            var result = {
                id: ++elem._blazorInputFileNextFileId,
                lastModified: new Date(blob.lastModified).toISOString(),
                name: fileName,
                size: blob.size,
                type: blob.type,
                relativePath: blob.webkitRelativePath
            };

            elem._blazorFilesById[result.id] = result;

            // Attach the blob data itself as a non-enumerable property so it doesn't appear in the JSON
            Object.defineProperty(result, 'blob', { value: blob });
            var fileList = [result];
            callbackWrapper.invokeMethodAsync('NotifyChange', fileList);
        }
        elem.addEventListener('change', function () {
            // Reduce to purely serializable data, plus an index by ID.
            const fileList = Array.prototype.map.call(elem.files, function (file) {
                const result = {
                    id: ++elem._blazorInputFileNextFileId,
                    lastModified: new Date(file.lastModified).toISOString(),
                    name: file.name,
                    size: file.size,
                    contentType: file.type,
                    readPromise: undefined,
                    arrayBuffer: undefined,
                };
                elem._blazorFilesById[result.id] = result;
                // Attach the blob data itself as a non-enumerable property so it doesn't appear in the JSON.
                Object.defineProperty(result, 'blob', { value: file });
                return result;
            });
            callbackWrapper.invokeMethodAsync('NotifyChange', fileList);
        });
    },
    toImageFile: async function toImageFile(elem, fileId) {
        var format = "image/jpeg";
        const originalFile = this.getFileById(elem, fileId);
        const loadedImage = await new Promise(function (resolve) {
            const originalFileImage = new Image();
            originalFileImage.onload = function () {
                resolve(originalFileImage);
            };
            originalFileImage.src = URL.createObjectURL(originalFile['blob']);
        });
        const resizedImageBlob = await new Promise(function (resolve) {
            var _a;
            const canvas = document.createElement('canvas');
            canvas.width = Math.round(loadedImage.width);
            canvas.height = Math.round(loadedImage.height);
            (_a = canvas.getContext('2d')) === null || _a === void 0 ? void 0 : _a.drawImage(loadedImage, 0, 0, canvas.width, canvas.height);
            canvas.toBlob(resolve, format, 0.95);
        });
        const result = {
            id: ++elem._blazorInputFileNextFileId,
            lastModified: originalFile.lastModified,
            name: originalFile.name,
            size: (resizedImageBlob === null || resizedImageBlob === void 0 ? void 0 : resizedImageBlob.size) || 0,
            contentType: format,
            readPromise: undefined,
            arrayBuffer: undefined,
        };
        elem._blazorFilesById[result.id] = result;
        // Attach the blob data itself as a non-enumerable property so it doesn't appear in the JSON.
        Object.defineProperty(result, 'blob', { value: resizedImageBlob });
        return result;
    },
    ensureArrayBufferReadyForSharedMemoryInterop : async function ensureArrayBufferReadyForSharedMemoryInterop(elem, fileId) {
        const arrayBuffer = await this.getArrayBufferFromFileAsync(elem, fileId);
        this.getFileById(elem, fileId).arrayBuffer = arrayBuffer;
    },
    readFileData: async function readFileData(elem, fileId, startOffset, count) {
        const arrayBuffer = await this.getArrayBufferFromFileAsync(elem, fileId);
        return btoa(String.fromCharCode.apply(null, new Uint8Array(arrayBuffer, startOffset, count)));
    },
    getFileById: function getFileById(elem, fileId) {
        const file = elem._blazorFilesById[fileId];
        if (!file) {
            throw new Error(`There is no file with ID ${fileId}. The file list may have changed.`);
        }
        return file;
    },
    getArrayBufferFromFileAsync: function getArrayBufferFromFileAsync(elem, fileId) {
        const file = this.getFileById(elem, fileId);
        // On the first read, convert the FileReader into a Promise<ArrayBuffer>.
        if (!file.readPromise) {
            file.readPromise = new Promise(function (resolve, reject) {
                const reader = new FileReader();
                reader.onload = function () {
                    resolve(reader.result);
                };
                reader.onerror = function (err) {
                    reject(err);
                };
                reader.readAsArrayBuffer(file['blob']);
            });
        }
        return file.readPromise;
    }
};
