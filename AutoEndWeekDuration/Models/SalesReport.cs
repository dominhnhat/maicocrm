﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoEndWeekDuration.Models
{
    public class SalesReport
    {
        public DateTime Month { get; set; }

        public double SalesGoals { get; set; }

        public int EmployeeId { get; set; }

        public Employee EmployeeNavigation { get; set; }

        public SalesReport(DateTime month, double salesGoals, int employeeId)
        {
            Month = month;
            SalesGoals = salesGoals;
            EmployeeId = employeeId;
        }
    }

    public class WeeklySalesReport
    {
        public int DurationId { get; set; }
        public int EmployeeId { get; set; }
        public double SalesGoals { get; set; }
        public WeeklySalesReport() { }
    }
    public class WeeklySalesReportDuration
    {
        public int Id { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime Month { get; set; }

        public WeeklySalesReportDuration(DateTime dateStart, DateTime? dateEnd, DateTime month)
        {
            DateStart = dateStart;
            DateEnd = dateEnd;
            Month = month;
        }
        public WeeklySalesReportDuration() { }

    }

}
