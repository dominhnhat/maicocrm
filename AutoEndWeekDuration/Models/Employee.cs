﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoEndWeekDuration.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int? OfficeId { get; set; }
        public virtual int DepartmentId { get; set; }
        public bool IsBlocked { get; set; }
    }
}
