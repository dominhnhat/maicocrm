﻿using AutoEndWeekDuration.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AutoEndWeekDuration.Services
{
    public class WeeklySalesReportServices
    {
        private readonly IConfiguration _configuration;
        public WeeklySalesReportServices(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Add(WeeklySalesReport weeklySalesReport)
        {
            using var connection = new NpgsqlConnection(_configuration.GetConnectionString("DefaultConnection"));
            string sql = "insert into \"WeeklySalesReports\"(duration_id, sales_goals, employee_id) values(@duration_id, @sales_goals, @employee_id)";
            var aff = connection.Execute(sql, new
            {
                duration_id = weeklySalesReport.DurationId,
                sales_goals = weeklySalesReport.SalesGoals,
                employee_id = weeklySalesReport.EmployeeId
            });
        }
    }
}
