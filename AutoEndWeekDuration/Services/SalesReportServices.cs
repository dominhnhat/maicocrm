﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using Npgsql;
using System.Threading.Tasks;
using AutoEndWeekDuration.Models;

namespace AutoEndWeekDuration.Services
{
    public class SalesReportServices
    {
        private readonly IConfiguration _configuration;
        public SalesReportServices(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task AddAsync(SalesReport salesReport)
        {
            using var connection = new NpgsqlConnection(_configuration.GetConnectionString("DefaultConnection"));
            string sql = "insert into \"SalesReports\"(month, sales_goals, employee_id) values(@month, @sales_goals, @employee_id)";
            await connection.ExecuteAsync(sql, new
            {
                month = salesReport.Month,
                sales_goals = salesReport.SalesGoals,
                employee_id = salesReport.EmployeeId
            });
        } 
    }
}
