﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;

namespace AutoEndWeekDuration.Services
{
    public class Utilities
    {
        public bool IsPhoneNumber(string number)
        {
            //string MatchPhoneNumberPattern = @"^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$";
            string matchPhoneNumberPattern = @"^\(?(?:\+?\d{2,3})\)?\d{8,13}$";
            if (number != null)
            {
                return Regex.IsMatch(number.Trim(), matchPhoneNumberPattern);
            }
            return false;
        }
        public bool IsEqual(object firstOrder, object secondOrder)
        {
            var jsonOfOrder = JsonConvert.SerializeObject(firstOrder, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            var jsonOfTempOrder = JsonConvert.SerializeObject(secondOrder, Formatting.Indented, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
            return jsonOfOrder == jsonOfTempOrder;
        }

        public DateTime GetCurrentDateTime()
        {
            DateTime dateTime = default!;
            TimeZoneInfo timeZone = null;
            // Determine if South Pole time zone is defined in system
            try
            {
                timeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            }
            // Time zone does not exist; create it, store it in a text file, and return it
            catch
            {
                const string filename = "TimeZoneInfo.txt";
                bool found = false;

                if (File.Exists(filename))
                {
                    StreamReader reader = new StreamReader(filename);
                    string timeZoneInfo;
                    while (reader.Peek() >= 0)
                    {
                        timeZoneInfo = reader.ReadLine();
                        if (timeZoneInfo.Contains("SE Asia Standard Time"))
                        {
                            timeZone = TimeZoneInfo.FromSerializedString(timeZoneInfo);
                            reader.Close();
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    // Define transition times to/from DST
                    TimeZoneInfo.TransitionTime startTransition = TimeZoneInfo.TransitionTime.CreateFloatingDateRule(new DateTime(1, 1, 1, 2, 0, 0), 10, 1, DayOfWeek.Sunday);
                    TimeZoneInfo.TransitionTime endTransition = TimeZoneInfo.TransitionTime.CreateFloatingDateRule(new DateTime(1, 1, 1, 2, 0, 0), 3, 3, DayOfWeek.Sunday);
                    // Define adjustment rule
                    TimeSpan delta = new TimeSpan(1, 0, 0);
                    TimeZoneInfo.AdjustmentRule adjustment = TimeZoneInfo.AdjustmentRule.CreateAdjustmentRule(new DateTime(1989, 10, 1), DateTime.MaxValue.Date, delta, startTransition, endTransition);
                    // Create array for adjustment rules
                    TimeZoneInfo.AdjustmentRule[] adjustments = { adjustment };
                    // Define other custom time zone arguments
                    string displayName = "(GMT+7:00) SE Asia Standard Time";
                    string standardName = "SE Asia Standard Time";
                    string daylightName = "SE Asia Daylight Time";
                    TimeSpan offset = new TimeSpan(7, 0, 0);
                    timeZone = TimeZoneInfo.CreateCustomTimeZone(standardName, offset, displayName, standardName, daylightName, adjustments);
                    // Write time zone to the file
                    StreamWriter writer = new StreamWriter(filename, true);
                    writer.WriteLine(timeZone.ToSerializedString());
                    writer.Close();
                }
            }
            dateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
            return dateTime;
        }
    }
}
