﻿using AutoEndWeekDuration.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AutoEndWeekDuration.Services
{

    public class EmployeeServices
    {
        private readonly IConfiguration _configuration;
        public EmployeeServices(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<List<Employee>> GetAllAsync()
        {
            using var connection = new NpgsqlConnection(_configuration.GetConnectionString("DefaultConnection"));
            return (List<Employee>)await connection.QueryAsync<Employee>("SELECT * FROM \"Employees\"");
        }
    }
}
