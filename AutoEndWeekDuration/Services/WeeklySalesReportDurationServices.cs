﻿using AutoEndWeekDuration.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AutoEndWeekDuration.Services
{
    public class WeeklySalesReportDurationServices
    {
        private readonly IConfiguration _configuration;
        public WeeklySalesReportDurationServices(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public WeeklySalesReportDuration GetLastest()
        {
            using var connection = new NpgsqlConnection(_configuration.GetConnectionString("DefaultConnection"));

            var maxId = connection.ExecuteScalar<int>("SELECT max(id) from \"WeeklySalesReportDurations\"");
            var durationLastest =  connection
                .QueryFirstOrDefault("SELECT * FROM \"WeeklySalesReportDurations\" WHERE id = @id",
                                                                                   new { id = maxId });


            return new WeeklySalesReportDuration()
            {
                Id = durationLastest.id,
                DateStart = durationLastest.date_start,
                DateEnd = durationLastest.date_end,
                Month = durationLastest.month
            };
        }

        public void Add(WeeklySalesReportDuration weeklySalesReportDuration)
        {
            using var connection = new NpgsqlConnection(_configuration.GetConnectionString("DefaultConnection"));
            string sql = "insert into \"WeeklySalesReportDurations\"(date_start, date_end, month) values(@date_start, @date_end, @month)";
            var affectedRows = connection.Execute(sql, new
            {
                date_start = weeklySalesReportDuration.DateStart,
                date_end = weeklySalesReportDuration.DateEnd,
                month = weeklySalesReportDuration.Month
            });
        }

    }
}
