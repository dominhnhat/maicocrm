using AutoEndWeekDuration.Models;
using AutoEndWeekDuration.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
namespace AutoEndWeekDuration
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IConfiguration _configuration;
        private EmployeeServices _employeeServices;
        private WeeklySalesReportDurationServices _weeklySalesReportDurationServices;
        private SalesReportServices _salesReportServices;
        private WeeklySalesReportServices _weeklySalesReportServices;
        private Utilities _utilities;
        public Worker(ILogger<Worker> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _employeeServices = new EmployeeServices(_configuration);
            _salesReportServices = new SalesReportServices(_configuration);
            _weeklySalesReportServices = new WeeklySalesReportServices(_configuration);
            _weeklySalesReportDurationServices = new WeeklySalesReportDurationServices(_configuration);
            _utilities = new Utilities();
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            DateTime currentDate = _utilities.GetCurrentDateTime();
            while (!stoppingToken.IsCancellationRequested)
            {
                await WeekEnd(currentDate);
                Console.WriteLine(currentDate);
                _logger.LogInformation("Worker running at: {0}", currentDate.ToLongDateString());
                await Task.Delay(1000 * 60 * 60 * 24, stoppingToken);
            }
        }
        async Task WeekEnd(DateTime date)
        {
            var maxDuration =  _weeklySalesReportDurationServices.GetLastest();
            var employees = await _employeeServices.GetAllAsync();

            DateTime monthStart = new DateTime();

            if (date.Day < 4 && date.Day >= 1)
            {
                monthStart = new DateTime(date.Year, date.AddMonths(-1).Month, 05);
            }
            else
            {
                monthStart = new DateTime(date.Year, date.Month, 05);
            }


            if (maxDuration != null)
            {
                if (date.Date == maxDuration.DateEnd.Value.Date)
                {
                    _weeklySalesReportDurationServices.Add(new WeeklySalesReportDuration(date.AddDays(1),
                                                                CreateDateEnd(date.AddDays(1)),
                                                                monthStart));
                    var durationId = _weeklySalesReportDurationServices.GetLastest().Id;
                    foreach (var item in employees)
                    {
                        WeeklySalesReport weeklySalesReport = new WeeklySalesReport();
                        _weeklySalesReportServices.Add(new WeeklySalesReport
                        {
                            DurationId = durationId,
                            EmployeeId = item.Id,
                            SalesGoals = 0
                        });
                    }
                }
            }

            if (date.Day == 5)
            {
                foreach (var item in employees)
                {
                    SalesReport salesReport = new SalesReport(monthStart, 0, item.Id);
                    await _salesReportServices.AddAsync(salesReport);
                }
            }
        }

        DateTime CreateDateEnd(DateTime dateStart)
        {
            while (true)
            {
                if (dateStart.DayOfWeek == DayOfWeek.Sunday)
                {
                    break;
                }
                dateStart = dateStart.AddDays(1);
            }
            return dateStart;
        }
    }
}
